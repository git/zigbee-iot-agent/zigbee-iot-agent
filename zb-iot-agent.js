var Nwkmgr = require('./hag-client/nwkmgr.js');
var ByteBuffer = require("bytebuffer"); 
var Webserver = require('./webserver/webserver.js');
  
// Constructor
function ZbIotAgent() {

  this.webserver = new Webserver();  
  this.devArray = [];
  
  var nwkmgr = new Nwkmgr();
  
  self = this;
  
  nwkmgr.on('newDev', function(data) {
   //console.log("nwkmgr emitted newDev: ", data);  
   //check we do not have the device already
   for(var devIdx in self.devArray)
   {
     if(self.devArray[devIdx].info.id === data.info.id)
     {
     	return;
     }	
   }
   self.devArray.push(data);
   self.webserver.upDateDeviceList(self.devArray);
  })
  .on('removeDev', function(data) {
   console.log("nwkmgr emitted removeDev: ", data);  
   //remove all devices with this ieee addr
   var devIdx = 0;
   for(devIdx = 0; devIdx < self.devArray.length; devIdx++)
   {
   	 var ieeeBb = new ByteBuffer(8, ByteBuffer.LITTLE_ENDIAN)
      .append(self.devArray[devIdx].info.ieee, "hex", 0);
   	 var ieeeRemoveBb = new ByteBuffer(8, ByteBuffer.LITTLE_ENDIAN)
      .append(data, "hex", 0);
            
   	 console.log("searching[", devIdx, "]: ", ieeeRemoveBb.toString('hex'), ":", ieeeBb.toString('hex'), ":", ieeeRemoveBb.toString('hex').localeCompare(ieeeBb.toString('hex')));
     if(ieeeRemoveBb.toString('hex').localeCompare(ieeeBb.toString('hex')) === 0)
     { 
     	 console.log("zb-iot-agent: removing device");    	
     	 var removeDevice = self.devArray.splice(devIdx, 1);       
       removeDevice[0].delete();
       delete removeDevice[0];
       //we just removed an array element, so reduce devIdx by 1;
       devIdx = devIdx-1;
     }	
   }
   self.webserver.upDateDeviceList(self.devArray);
  });

  this.webserver.on('addDevice', function(data) {
   console.log("webserver emitted addDevice: ");  
   nwkmgr.openNetwork(60);
  }).on('addDeviceDone', function(data) {
   console.log("webserver emitted addDeviceDone: ");  
   nwkmgr.openNetwork(0);
  }).on('removeDevice', function(data) {
   console.log("webserver emitted removeDevice: ", data);  	
   var devIdx = 0;
   var found = false;
   for(devIdx = 0; devIdx < self.devArray.length; devIdx++)
   {
   	 var ieeeBb = new ByteBuffer(8, ByteBuffer.LITTLE_ENDIAN)
      .append(self.devArray[devIdx].info.ieee, "hex", 0);
   	 var ieeeRemoveBb = new ByteBuffer(8, ByteBuffer.LITTLE_ENDIAN)
      .append(data, "hex", 0);
            
   	 console.log("searching[", devIdx, "]: ", ieeeRemoveBb.toString('hex'), ":", ieeeBb.toString('hex'), ":", ieeeRemoveBb.toString('hex').localeCompare(ieeeBb.toString('hex')));
     if(ieeeRemoveBb.toString('hex').localeCompare(ieeeBb.toString('hex')) === 0)
     {     	
     	 console.log("found: ", ieeeRemoveBb.buffer);  	
     	 //only send network leave for 1 endpoint
     	 if( found === false) {
         nwkmgr.removeDev(self.devArray[devIdx].info.ieee);
       }
       var removeDevice = self.devArray.splice(devIdx, 1);       
       removeDevice[0].delete();
       delete removeDevice[0];
       //we just removed an array element, so reduce devIdx by 1;
       devIdx = devIdx-1;
       found = true;
     
     }	
   }
  });
    
  //create the devices
  nwkmgr.getDevList();

}

var zbIotAgent = new ZbIotAgent();

