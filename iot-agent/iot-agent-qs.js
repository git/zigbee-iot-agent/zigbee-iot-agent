var events = require('events');
var mqtt = require('mqtt');
  
// Constructor
function IotAgentQs(devInfo) {
	
  //console.log("IotAgentQs: new device ", devInfo);
  
  events.EventEmitter.call(this);

  var ia = this;
 
  var data_topic = "iot-2/evt/status/fmt/json";
  var cmd_topic = "iot-2/cmd/+/fmt/json";
  var broker = "ws://" + devInfo.prov.org + ".messaging.internetofthings.ibmcloud.com:1883";
                                  
  var clientId = "d:" +  devInfo.prov.org + ":" + devInfo.prov.type + ":" + devInfo.id;
  var Username = "use-token-auth";
  var Password =  devInfo.prov.auth_token;   
  
  var options = {
    clientId: clientId,
    keepalive : 30
  };
  
  if(devInfo.prov.auth_token !== '')
  {
    options.username = "use-token-auth";
    options.password = devInfo.prov.auth_token;
  }
  
  console.log('mqtt connect');

  var client = mqtt.connect(broker, options);

  if(devInfo.prov.auth_token !== '')
  {
    client.subscribe(cmd_topic);
  }

  client.on('message', function (topic, message) {
    var messageJsn = JSON.parse(message);
    console.log("IotAgent: got message:", message.toString());
    if(typeof messageJsn.a !== 'undefined')
    {    
      ia.emit('data', messageJsn.a);
    }
  })
  client.on('error', function (err) {
  	console.log("IotAgent error: ", err);
  	//do nothing, it should reconnect when connection is available
  }),
  client.on('connect', function () {
        console.log("IotAgent["+clientId+"]: connect");
        //do nothing, it should reconnect when connection is available
  }),
    client.on('disconnect', function () {
        console.log("IotAgent["+clientId+"]: disconnect");
        //do nothing, it should reconnect when connection is available
  }),
  client.on('reconnect', function () {
        console.log("IotAgent["+clientId+"]: reconnect");
        //do nothing, it should reconnect when connection is available
  }),
  client.on('close', function () {
        console.log("IotAgent["+clientId+"]: close");
        //do nothing, it should reconnect when connection is available
  });   
   
  // public class methods
  this.sendMessage = function(message) {
     var devEvent = {
       d: message
    };
    
    //console.log("IotAgent["+clientId+"]: sending message:", JSON.stringify(devEvent));
  	
    client.publish(data_topic, JSON.stringify(devEvent));
  };
  
  this.delete = function () {
    client.end();
  }
  
  return ia;
}

IotAgentQs.prototype.__proto__ = events.EventEmitter.prototype;
 
// export the class
module.exports = IotAgentQs;


