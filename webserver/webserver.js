var http = require('http');
var socket_io = require('socket.io');
var fs = require('fs');
var events = require('events');
var url = require("url");

// Constructor
function Webserver() {
  
  events.EventEmitter.call(this);
  
  this.devList = [];

  app = http.createServer(handler);
  this.io = socket_io.listen(app);
  app.listen(5000);
  // socket.io options go here
  this.io.set('log level', 2);   // reduce logging - set 1 for warn, 2 for info, 3 for debug
  this.io.set('browser client minification', true);  // send minified client
  this.io.set('browser client etag', true);  // apply etag caching logic based on version number

  webserverInstance = this;
  self = this;
  
  console.log('webserver: Server running on http://' + getIPAddress() + ':5000');

  function handler (req, res) {
  	var pathname = url.parse(req.url).pathname;
    console.log("Request for " + pathname + " received.");

    if( (pathname.indexOf("devicelist.js") > -1 ) ||
        (pathname.indexOf("device-view.html") > -1 ) ||
        (pathname.indexOf("device-view.js") > -1 ) || 
        (pathname.indexOf("light-device-view.html") > -1 ) ||
        (pathname.indexOf("light-device-view.js") > -1 ) ||        
        (pathname.indexOf("temp-device-view.html") > -1 ) ||
        (pathname.indexOf("temp-device-view.js") > -1 ) ||  
        (pathname.indexOf("lightsensor-device-view.html") > -1 ) ||
        (pathname.indexOf("lightsensor-device-view.js") > -1 ) ||  
        (pathname.indexOf("switch-device-view.html") > -1 ) ||
        (pathname.indexOf("switch-device-view.js") > -1 ) || 
        (pathname.indexOf("farbtastic.css") > -1 ) ||        
        (pathname.indexOf("farbtastic.js") > -1 ) ||        
        (pathname.indexOf("menu-pannel.css") > -1 ) ||
        (pathname.indexOf("wheel.png") > -1 ) ||        
        (pathname.indexOf("mask.png") > -1 ) ||        
        (pathname.indexOf("marker.png") > -1 ) ){
        	
      pathname = "webserver/" + pathname;   	
      console.log("loading: ", pathname);
      fs.readFile(pathname,    // load file
      function (err, data) {
        if (err) {
          res.writeHead(500);
          return res.end('Error loading index.html');
        }
        res.writeHead(200);
        res.end(data);
      });
    }
    else
    {
      fs.readFile('webserver/deviceList.html',    // load html file
      function (err, data) {
        if (err) {
          res.writeHead(500);
          return res.end('Error loading index.html');
        }
        res.writeHead(200);
        res.end(data);
      }); 
    }       
  }

  webserverInstance.io.sockets.on('connection', function (socket) {
    socket
    .on('addDevice', function (data) {
      console.log("webserver: add device");
      webserverInstance.emit('addDevice');
    })
    .on('addDeviceDone', function (data) {
      console.log("webserver: add device done");
      webserverInstance.emit('addDeviceDone');
    })
    .on('getDeviceList', function (data) {
      //console.log("webserver: sending device list: ", webserverInstance.devList);
      socket.emit('devList', webserverInstance.devList);             
    })
    .on('devSet', function (data) {
      console.log("webserver devSet: ", data);
      for(var devIdx in webserverInstance.devList){
        if(webserverInstance.devList[devIdx].info.id === data.id) {
          webserverInstance.devList[devIdx].devSet(data);
        }      		
      }             
    })        
    .on('setDevInfo', function (data) {
      console.log("webserver setDevInfo: ", data);
      for(var devIdx in webserverInstance.devList){
        if(webserverInstance.devList[devIdx].info.id === data.id) {          
          webserverInstance.devList[devIdx].devSetInfo(data);
        }      		
      }             
    })  
    .on('removeDevice', function (data) {
    	console.log("webserver: removeDevice: ", data);
      webserverInstance.emit('removeDevice', data);         
    })  
    .on('error', function (err) {
      console.log("webserver: socket error: ", err);
    });
  });

  // Get server IP address on LAN
  function getIPAddress() {
    var interfaces = require('os').networkInterfaces();
    for (var devName in interfaces) {
      var iface = interfaces[devName];
      for (var i = 0; i < iface.length; i++) {
        var alias = iface[i];
        if (alias.family === 'IPv4' && alias.address !== '127.0.0.1' && !alias.internal)
          return alias.address;
      }
    }
    return '0.0.0.0';
  }
  
  this.upDateDeviceList = function(devList) { 
  	 //console.log("webserver: got device list: ", devList);            
     webserverInstance.devList = devList;
     webserverInstance.io.sockets.emit('devList', webserverInstance.devList);     
  };
  
  this.test = function()
  {
  	  //self.emit('addDevice');
  };
}

Webserver.prototype.__proto__ = events.EventEmitter.prototype;

// export the class
module.exports = Webserver;
