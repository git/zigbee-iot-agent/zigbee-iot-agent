
function DeviceListClient() {
       
    
    self = this;
    self.socket = io.connect();
    self.deviceList;
    self.prevDeviceList;
    self.AddNewDevMode = false; 

    DeviceListClient.prototype.addDevice = function(sel) {   
      self.socket.emit('addDevice');
    };
    
    DeviceListClient.prototype.getDeviceList = function() { 
      self.socket.emit('getDeviceList');
    };
    
    self.socket.on('devList', function (devList) {
      self.deviceList = devList;
      self.drawList();
      if(self.AddNewDevMode === true)
      {
        self.drawAddDevList();
      }
    }); 
    
    DeviceListClient.prototype.drawList = function(deviceList) { 
      var li = '<li data-role="list-divider" data-theme="a">Device List [name : id]</li>';     
      var devHtml;
      for(var i in self.deviceList) { 
      	if(self.deviceList[i].data.type === "TempSensor")
        {
        	devHtml = "temp-device-view.html"
        }
      	else if(self.deviceList[i].data.type === "LightSensor")
        {
        	devHtml = "lightsensor-device-view.html"
        }        
        else if( (self.deviceList[i].data.type === "ColorLight") ||
             (self.deviceList[i].data.type === "DimmableLight") ||
             (self.deviceList[i].data.type === "OnOffLight") )      	
        {
          devHtml = "light-device-view.html"
        }
        else if(self.deviceList[i].data.type === "OnOffSwitch")      	
        { 
          devHtml = "switch-device-view.html"
        }       
        else
        {
        	devHtml = "device-view.html"
        }
         	
      	li += "<li><a href=\"" + devHtml + "?devId=" + 
      	self.deviceList[i].info.id + 
      	"\" target=\"_top\" data-theme=\"a\">" +
      	self.deviceList[i].data.name + " : " + 
      	self.deviceList[i].info.id + "</a></li>";
      	
      } 
      $('#devList').html(li);
      
      $('#devList').html(li).promise().done(function () {
        //refresh here - $(this) refers to ul here
        $(this).listview("refresh");
        //causes a refresh to happen on the elements such as button etc. WHICH lie inside ul
        $(this).trigger("create");
      });
    };
    
    DeviceListClient.prototype.drawAddDevList = function(deviceList) { 
      var li = ''; 
      var oldDev = false;          
      for(var i in self.deviceList) {      	
        //check it is not already in the list
        oldDev = false;
        for(var j in self.prevDeviceList) { 
          if(self.deviceList[i].info.id === self.prevDeviceList[j].info.id)
          {
          	oldDev = true;
          }
        } 
        if(oldDev === false)
        {
      	  li += "<li><a href=\"#\">"  + self.deviceList[i].data.name + " : " + self.deviceList[i].info.id + "</a></li>";
      	}
      } 
      if(li !== '')
      {
        $('#addDevDevList').html(li);
      
        $('#addDevDevList').html(li).promise().done(function () {
          //refresh here - $(this) refers to ul here
          $(this).listview("refresh");
          //causes a refresh to happen on the elements such as button etc. WHICH lie inside ul
          $(this).trigger("create");
        });
      }                 
    };
 }