
function DeviceViewClient() {
         
  var urlParams = window.location.search.substring(1);  
  self = this;
  self.devId;
  self.device;
  self.socket = io.connect();
 
  self.socket.emit('getDeviceList');  
    
  self.socket.on('devList', function (devList) {
    for(var devIdx in devList)
    {
  	  if(devList[devIdx].info.id === self.devId) {
  	    self.device = devList[devIdx];
  	   	  
  	    self.updateForm();
  	  }
  	}
  	  
  });
      
  if(urlParams.indexOf("&") > -1){
    self.devId = urlParams.substring(urlParams.indexOf("devId=")+6, urlParams.indexOf("&"));
  }
  else {
  	self.devId = urlParams.substring(urlParams.indexOf("devId=")+6);
  }
    
  DeviceViewClient.prototype.updateForm = function() { 

    //update the device info fields
    $( '#deviceId' ).val(self.device.info.id);
    $( '#deviceName' ).val(self.device.data.type);
    $( '#deviceType' ).val(self.device.data.name);
        
    //update qs button link
    var qsUrl = 'https://quickstart.internetofthings.ibmcloud.com/#/device/' +
              self.device.info.id + '/sensor/temp';
      	  
    $("a[href='https://quickstart.internetofthings.ibmcloud.com/']").attr('href', qsUrl);
    
    //update back button
    var backUrl = 'http://' + location.host + '/';
    $("a[href='http://beaglebone.local:5000/']").attr('href', backUrl);
        
    $( '#temperatureText' ).val(self.device.data.temp);
    
    //calculate the color of the temp text box
    var r=0;
    var b=0;
    if(self.device.data.temp > 30) {
      r = 0xFF;
      b = 0;
    }
    else if(self.device.data.temp < 10) {
      r = 0;
      b = 0xFF;      
    }
    else {
      //temp is between 10 and 30
      var t = self.device.data.temp - 10;
      r = t * (255 / 20);
      b = 255 - (t * (255 / 20));
    }
    	
    //make sure there are 2 digits for the color formating	
    var rStr = Math.floor(r) >= 10 ? Math.floor(r).toString(16) : "0"+ Math.floor(r).toString(16);
    var bStr = Math.floor(b) >= 10 ? Math.floor(b).toString(16) : "0"+ Math.floor(b).toString(16);
    	
    //update the color of the tmp text box
    $( '#temperatureText' ).css('background-color' ,'#' + rStr + '00' + bStr );
    
  };
  
  //start a timer to update the temp
  setInterval(function () {
    //update temp
    self.socket.emit('getDeviceList'); 
  }, 5000); 
}
