
function DeviceViewClient() {
         
  var urlParams = window.location.search.substring(1);  
  self = this;
  self.devId;
  self.device;
  self.socket = io.connect();
 
  self.socket.emit('getDeviceList');  
    
  self.socket.on('devList', function (devList) {
    for(var devIdx in devList)
    {
  	  if(devList[devIdx].info.id === self.devId) {
  	    self.device = devList[devIdx];
  	   	  
  	    self.updateForm();
  	  }
  	}
  	  
  });
      
  if(urlParams.indexOf("&") > -1){
    self.devId = urlParams.substring(urlParams.indexOf("devId=")+6, urlParams.indexOf("&"));
  }
  else {
  	self.devId = urlParams.substring(urlParams.indexOf("devId=")+6);
  }
    
  DeviceViewClient.prototype.updateForm = function() { 
    
    //update the device info fields
    $( '#deviceId' ).val(self.device.info.id);
    $( '#deviceName' ).val(self.device.data.type);
    $( '#deviceType' ).val(self.device.data.name);
        
    //update qs button link to go to the 'on' attribute
    var qsUrl = 'https://quickstart.internetofthings.ibmcloud.com/#/device/' +
              self.device.info.id + '/sensor/on';      	  
    $("a[href='https://quickstart.internetofthings.ibmcloud.com/']").attr('href', qsUrl);
    
    //update back button
    var backUrl = 'http://' + location.host + '/';
    $("a[href='http://beaglebone.local:5000/']").attr('href', backUrl);
    
    if(self.device.data.on === 1) {
      $( '#stateSwitch' ).val("on");
    }
    else{
      $( '#stateSwitch' ).val("off");    
    }
    $( '#stateSwitch' ).slider('refresh');       
  }
  
    //start a timer to update the temp
  setInterval(function () {
    //update temp
    self.socket.emit('getDeviceList'); 
  }, 1000); 
}
