
function DeviceViewClient() {
         
  var urlParams = window.location.search.substring(1);  
  self = this;
  self.devId;
  self.device;
  self.socket = io.connect();
 
  self.socket.emit('getDeviceList');  
    
  self.socket.on('devList', function (devList) {
    for(var devIdx in devList)
    {
  	  if(devList[devIdx].info.id === self.devId) {
  	    self.device = devList[devIdx];
  	   	  
  	    self.updateForm();
  	  }
  	}
  	  
  });
      
  if(urlParams.indexOf("&") > -1){
    self.devId = urlParams.substring(urlParams.indexOf("devId=")+6, urlParams.indexOf("&"));
  }
  else {
  	self.devId = urlParams.substring(urlParams.indexOf("devId=")+6);
  }
    
  DeviceViewClient.prototype.updateForm = function() { 
    
    //update the device info fields
    $( '#deviceId' ).val(self.device.info.id);
    $( '#deviceName' ).val(self.device.data.type);
    $( '#deviceType' ).val(self.device.data.name);
        
    //update qs button link
    var qsUrl = 'https://quickstart.internetofthings.ibmcloud.com/#/device/' +
              self.device.info.id + '/sensor/';
    if(self.device.data.type === "TempSensor")
    {
      //make the temp show on the graph
      qsUrl = qsUrl + 'temp';
    }
    else if( (self.device.data.type === "ColorLight") ||
             (self.device.data.type === "DimmableLight") ||
             (self.device.data.type === "OnOffLight") ||
             (self.device.data.type === "OnOffSwitch") )      	
    {
      //make the 'on' show on the graph
      qsUrl = qsUrl + 'on';
    }
      	  
    $("a[href='https://quickstart.internetofthings.ibmcloud.com/']").attr('href', qsUrl);
    
    if(self.device.data.on === 1) {
      $( '#stateSwitch' ).val("on");
    }
    else{
      $( '#stateSwitch' ).val("off");    
    }
    $( '#stateSwitch' ).slider('refresh');
    
    //bind on/off switch callback    
    $( '#stateSwitch' ).bind( "change", function(event, ui) {
    	var on;
    	if($( '#stateSwitch' ).val() === 'on') {
    	  on = 1;
    	}
    	else {
        on = 0;
      }
    			
    	var data = {
    		id: self.device.info.id,
    		on: on  
    	};
    	
    	self.socket.emit('devSet', data);
    	     
    });
    
    //color picker
     //COLOUR PICKER
    var palletteColour = new Object;

    document.getElementById("pickerHue").value=self.device.data.hue;
    document.getElementById("pickerSat").value=self.device.data.saturation;
    document.getElementById("pickerLev").value=self.device.data.level;
    updatePickerFromTable();
            
    $("#pickerHue").keyup(updatePickerFromTable);
    $("#pickerSat").keyup(updatePickerFromTable);
    $("#pickerLev").keyup(updatePickerFromTable);
    
    self.prevHue = document.getElementById("pickerHue").value;
    self.prevsat = document.getElementById("pickerSat").value;
    self.prevLevel = document.getElementById("pickerLev").value;
        
    function updatePickerFromTable (event){
        var f = $.farbtastic('#picker');
        f.setHSV([document.getElementById("pickerHue").value/255,
                  document.getElementById("pickerSat").value/255,
                  document.getElementById("pickerLev").value/255]);
		document.getElementById("infoSwatch").value=f.color;		  
        document.getElementById("infoSwatch").style.backgroundColor=f.color;
        
        console.log("updatew color");
		
    };

    $(document).ready(function() {
        
        var f = $.farbtastic('#picker');
        updatePickerFromTable();

        f.linkTo(function(color) {
            palletteColour.rgb = f.color;
            var hsv = {
                h:(f.hsv[0]*255).toFixed(0),
                s:(f.hsv[1]*255).toFixed(0),
                v:(f.hsv[2]*255).toFixed(0)
            };

            //console.log(hsv);
            palletteColour.hsv = hsv;
            //console.log(palletteColour);
            //console.log(palletteColour.hsv.h);
            //console.log(numToHex(palletteColour.hsv.h*255, 2));
            //console.log("selected triangles = " + selectedTriangles);

            document.getElementById("pickerHue").value=hsv.h;
            document.getElementById("pickerSat").value=hsv.s;
            document.getElementById("pickerLev").value=hsv.v;
            document.getElementById("infoSwatch").value=f.color;
            document.getElementById("infoSwatch").style.backgroundColor=f.color;
			
			if( (parseInt(hsv.h) > (parseInt(self.prevHue) + 5)) ||
          (parseInt(hsv.h) < (parseInt(self.prevHue) - 5)) ||
          (parseInt(hsv.s) > (parseInt(self.prevSat) + 5)) ||
          (parseInt(hsv.s) < (parseInt(self.prevSat) - 5)))  {
        
        var hue = parseInt(hsv.h);  
        var data = {
          id: self.device.info.id,
          hue: parseInt(hsv.h),
          sat: parseInt(hsv.s)    
    	  };
    	
    	  self.socket.emit('devSet', data);
    	  
    	  self.prevHue = hsv.h;
    	  self.prevSat = hsv.s;
      }

      if( (parseInt(hsv.v) > (parseInt(self.prevLevel) + 5)) ||
          (parseInt(hsv.v) < (parseInt(self.prevLevel) - 5)) ) {
        
        var level = parseInt(hsv.v);  
        var data = {
          id: self.device.info.id,
          level: parseInt(hsv.v)  
    	  };
    	
    	  self.socket.emit('devSet', data);
    	  
    	  self.prevLevel = hsv.v;	
      }      
    });
  });

/*
//STRING AND COLOUR SPACE MANIPULATIONS
function hsvToRgb(h, s, l){
    var r, g, b;
    
    if(s == 0){
        r = g = b = l; // achromatic
    }else{
        function hue2rgb(p, q, t){
            if(t < 0) t += 1;
            if(t > 1) t -= 1;
            if(t < 1/6) return p + (q - p) * 6 * t;
            if(t < 1/2) return q;
            if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
            return p;
        }

        var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
        var p = 2 * l - q;
        r = hue2rgb(p, q, h + 1/3);
        g = hue2rgb(p, q, h);
        b = hue2rgb(p, q, h - 1/3);
    }

    return [r * 255, g * 255, b * 255];
}   

function rgbToHsv(r, g, b){
  r /= 255, g /= 255, b /= 255;
  var max = Math.max(r, g, b), min = Math.min(r, g, b);
  var h, s, l = (max + min) / 2;

  if(max == min){
    h = s = 0; // achromatic
  }else{
    var d = max - min;
    s = l > 0.5 ? d / (2 - max - min) : d / (max + min);
    switch(max){
      case r: h = (g - b) / d + (g < b ? 6 : 0); break;
      case g: h = (b - r) / d + 2; break;
      case b: h = (r - g) / d + 4; break;
    }
    h /= 6;
  }
h=Math.round(h*255);
s=Math.round(s*255);
l=Math.round(l*255);
return [h, s, l];
}

function rgbStringToRgb(string){
  var bracket1 = string.indexOf("(");
	var bracket2 = string.lastIndexOf(")");
	var comma1 = string.indexOf(",");
	var comma2 = string.lastIndexOf(",");
	//console.log(bracket1);
	//console.log(bracket2);
	//console.log(comma1);
	//console.log(comma2);
	var r = string.slice(bracket1+1,comma1);
	//console.log(r);
	var g = string.slice(comma1+1,comma2);
	var b = string.slice(comma2+1,bracket2);
	
	return [parseInt(r),parseInt(g),parseInt(b)];

};

function rgbStringToHsv(rgbString){
	var rgb=rgbStringToRgb(rgbString);
	return rgbToHsv(rgb[0],rgb[1],rgb[2]);

};


function hsvToRgb(h, s, l){
  h /= 255, s /= 255, l /= 255;
  var r, g, b;
  
  if(s == 0){
    r = g = b = l; // achromatic
  }else{
    function hue2rgb(p, q, t){
      if(t < 0) t += 1;
      if(t > 1) t -= 1;
      if(t < 1/6) return p + (q - p) * 6 * t;
      if(t < 1/2) return q;
      if(t < 2/3) return p + (q - p) * (2/3 - t) * 6;
      return p;
    }

    var q = l < 0.5 ? l * (1 + s) : l + s - l * s;
    var p = 2 * l - q;
    r = hue2rgb(p, q, h + 1/3);
    g = hue2rgb(p, q, h);
    b = hue2rgb(p, q, h - 1/3);
  }

  return [Math.round(r * 255), Math.round(g * 255), Math.round(b * 255)];
}   

function rgbToRgbString(r,g,b){
  return ("33.15px solid rgb(" + r.toString() + "," + g.toString() + "," + b.toString() + ")");
}

function hsvToRgbString(h,s,l){
  var rgb = hsvToRgb(h, s, l);
  return rgbToRgbString(rgb[0],rgb[1],rgb[2])
}   

*/

}

}