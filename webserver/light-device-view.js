
function DeviceViewClient() {
         
  var urlParams = window.location.search.substring(1);  
  self = this;
  self.devId;
  self.device;
  self.socket = io.connect();
 
  self.socket.emit('getDeviceList');  
    
  self.socket.on('devList', function (devList) {
    for(var devIdx in devList)
    {
  	  if(devList[devIdx].info.id === self.devId) {
  	    self.device = devList[devIdx];
  	   	  
  	    self.updateForm();
  	  }
  	}
  	  
  });
      
  if(urlParams.indexOf("&") > -1){
    self.devId = urlParams.substring(urlParams.indexOf("devId=")+6, urlParams.indexOf("&"));
  }
  else {
  	self.devId = urlParams.substring(urlParams.indexOf("devId=")+6);
  }
    
  DeviceViewClient.prototype.updateForm = function() { 
    
    //update the device info fields
    $( '#deviceId' ).val(self.device.info.id);
    $( '#deviceName' ).val(self.device.data.type);
    $( '#deviceType' ).val(self.device.data.name);
        
    //update qs button link
    var qsUrl = 'https://quickstart.internetofthings.ibmcloud.com/#/device/' +
              self.device.info.id + '/sensor/';
    if(self.device.data.type === "TempSensor")
    {
      //make the temp show on the graph
      qsUrl = qsUrl + 'temp';
    }
    else if( (self.device.data.type === "ColorLight") ||
             (self.device.data.type === "DimmableLight") ||
             (self.device.data.type === "OnOffLight") ||
             (self.device.data.type === "OnOffSwitch") )      	
    {
      //make the 'on' show on the graph
      qsUrl = qsUrl + 'on';
    }
      	  
    $("a[href='https://quickstart.internetofthings.ibmcloud.com/']").attr('href', qsUrl);
    
    //update back button
    var backUrl = 'http://' + location.host + '/';
    $("a[href='http://beaglebone.local:5000/']").attr('href', backUrl);


    if(self.device.data.on === 1) {
      $( '#stateSwitch' ).val("on");
    }
    else{
      $( '#stateSwitch' ).val("off");    
    }
    $( '#stateSwitch' ).slider('refresh');
    
    //bind on/off switch callback    
    $( '#stateSwitch' ).bind( "change", function(event, ui) {
    	var on;
    	if($( '#stateSwitch' ).val() === 'on') {
    	  on = 1;
    	}
    	else {
        on = 0;
      }
    			
    	var data = {
    		id: self.device.info.id,
    		on: on  
    	};
    	
    	self.socket.emit('devSet', data);
    	     
    });
    
    //color picker
     //COLOUR PICKER
    var palletteColour = new Object;

    document.getElementById("pickerHue").value=self.device.data.hue;
    document.getElementById("pickerSat").value=self.device.data.saturation;
    document.getElementById("pickerLev").value=self.device.data.level;
    updatePickerFromTable();
            
    $("#pickerHue").keyup(updatePickerFromTable);
    $("#pickerSat").keyup(updatePickerFromTable);
    $("#pickerLev").keyup(updatePickerFromTable);
    
    self.prevHue = document.getElementById("pickerHue").value;
    self.prevsat = document.getElementById("pickerSat").value;
    self.prevLevel = document.getElementById("pickerLev").value;
        
    function updatePickerFromTable (event){
        var f = $.farbtastic('#picker');
        f.setHSV([document.getElementById("pickerHue").value/255,
                  document.getElementById("pickerSat").value/255,
                  document.getElementById("pickerLev").value/255]);
		document.getElementById("infoSwatch").value=f.color;		  
        document.getElementById("infoSwatch").style.backgroundColor=f.color;
        
        console.log("updatew color");
		
    };

    $(document).ready(function() {
        
        var f = $.farbtastic('#picker');
        updatePickerFromTable();

        f.linkTo(function(color) {
            palletteColour.rgb = f.color;
            var hsv = {
                h:(f.hsv[0]*255).toFixed(0),
                s:(f.hsv[1]*255).toFixed(0),
                v:(f.hsv[2]*255).toFixed(0)
            };

            //console.log(hsv);
            palletteColour.hsv = hsv;
            //console.log(palletteColour);
            //console.log(palletteColour.hsv.h);
            //console.log(numToHex(palletteColour.hsv.h*255, 2));
            //console.log("selected triangles = " + selectedTriangles);

            document.getElementById("pickerHue").value=hsv.h;
            document.getElementById("pickerSat").value=hsv.s;
            document.getElementById("pickerLev").value=hsv.v;
            document.getElementById("infoSwatch").value=f.color;
            document.getElementById("infoSwatch").style.backgroundColor=f.color;
			
			if( (parseInt(hsv.h) > (parseInt(self.prevHue) + 5)) ||
          (parseInt(hsv.h) < (parseInt(self.prevHue) - 5)) ||
          (parseInt(hsv.s) > (parseInt(self.prevSat) + 5)) ||
          (parseInt(hsv.s) < (parseInt(self.prevSat) - 5)))  {
        
        var hue = parseInt(hsv.h);  
        var data = {
          id: self.device.info.id,
          hue: parseInt(hsv.h),
          saturation: parseInt(hsv.s)    
    	  };
    	
    	  self.socket.emit('devSet', data);
    	  
    	  self.prevHue = hsv.h;
    	  self.prevSat = hsv.s;
      }

      if( (parseInt(hsv.v) > (parseInt(self.prevLevel) + 5)) ||
          (parseInt(hsv.v) < (parseInt(self.prevLevel) - 5)) ) {
        
        var level = parseInt(hsv.v);  
        var data = {
          id: self.device.info.id,
          level: parseInt(hsv.v)  
    	  };
    	
    	  self.socket.emit('devSet', data);
    	  
    	  self.prevLevel = hsv.v;	
      }      
    });
  });

}

}