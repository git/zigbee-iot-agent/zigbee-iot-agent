var ByteBuffer = require("bytebuffer"); 
var Hagateway = require('../hag-client/hagateway.js');
var IotAgent = require('../iot-agent/iot-agent-qs.js');
var storage = require('node-persist');

// Constructor
function LightDevice(_nwkmgr, ieee, ep, type) {
  
  var ld = this;
  
  var nwkmgr = _nwkmgr;  
  
  var ieeeBb = new ByteBuffer(8, ByteBuffer.LITTLE_ENDIAN)
  .append(ieee, "hex", 0)
  .reverse();
  
  //id is first 3 byes of ieee, last 2 bytes of ieee and 
  //ep with 2 digits i.e. ep of 8 = 08
  ld.info = {
  	id: ieeeBb.toString('hex').substring(0,4) + ieeeBb.toString('hex').substring(10,16) + (("0" + ep).slice(-2)),
  	ieee: ieee,
  	ep: ep,
  };
        
  //read stored provisioning info  
  storage.initSync();
  ld.info.prov = storage.getItem(ld.info.id);
 
  //if it is not stored point it at quickstart
  if(typeof ld.info.prov === 'undefined') {
    ld.info.prov = {
      org: "quickstart",
      type: "zb",
      auth_token: ""
    }	
  }
    
  // initialize data properties
  ld.data = { 
  	guid: ieeeBb.toString('hex') + ":" + ld.info.ep,
  	name: "ZigBee Light", //TODO: Look at manu ID and prod ID to name it correctly 
  	type: type 	
  };
  
  switch (type) {
    case "ColorLight":
      ld.data.hue = 0;
      ld.data.saturation = 0;
    case "DimmableLight":
      ld.data.level = 0;
      ld.data.transitionTime = 10;
    default:
      //default attributes for all light devices
      ld.data.on = 0;
      break;
  }

  //start the report timer
  ld.data.reportInterval = 5000;  
  var reportTimer = setInterval(function() { devReport(hagateway, ld); }, ld.data.reportInterval);     
  
  //create the connection to the HA Gateway  
  var hagateway = new Hagateway();
  hagateway
  .on('state', function(data) {
    //console.log("LightDevice: got hagateway message:", data);
    
    //if its data for ld device
    if( (data.ieee.toString('hex') === ld.info.ieee.toString('hex')) && (data.ep === ld.info.ep) )
    {
      //console.log("LightDevice: got hagateway state message - iotAgent: ", iotAgent.clientId);
      if(data.status === 0)
      {
        ld.data.on = data.on;    	
        iotAgent.sendMessage(ld.data);
        console.log(ld.data.guid, ": state rsp ", data.status, ":", data.seqNum);
      }
      else
      {
      	console.log(ld.data.guid, ": state rsp error ", data.status, ":", data.seqNum);
      }
    }
  })
  .on('level', function(data) {
    //console.log("LightDevice: got hagateway level:", data);
    
    //if its data for ld device
    if( (data.ieee.toString('hex') === ld.info.ieee.toString('hex')) && (data.ep === ld.info.ep) )
    {
      //console.log("LightDevice: got hagateway message:", data);
      if(data.status === 0)
      {
        ld.data.level = data.level;    	
        console.log(ld.data.guid, ": level rsp ", data.status, ":", data.seqNum);
      }
      else
      {
      	console.log(ld.data.guid, ": level rsp error ", data.status, ":", data.seqNum);
      }
    }
  })
  .on('color', function(data) {
    //console.log("LightDevice: got hagateway level:", data);
    
    //if its data for ld device
    if( (data.ieee.toString('hex') === ld.info.ieee.toString('hex')) && (data.ep === ld.info.ep) )
    {
      //console.log("LightDevice: got hagateway message:", data);
      if(data.status === 0)
      {
        ld.data.hue = data.hue;  
        ld.data.saturation = data.saturation;  	
        console.log(ld.data.guid, ": color rsp ", data.status, ":", data.seqNum);
      }
      else
      {
      	console.log(ld.data.guid, ": color rsp error ", data.status, ":", data.seqNum);
      }
    }
  });

  //create the connection to the IoT Agent 
  var iotAgent = new IotAgent(ld.info);
  
  //console.log("created client id: ", iotAgent.clientId);
  
  iotAgent.on('data', function(data) {
  	ld.devSet(data);
  });
  
  //private methods with private member access    
  function devReport(hagateway, device)
  {
  	//send read request for periodic report
    //console.log("LightDev", ld.info.ieee.toString("hex"), ":", ld.info.ep, " report");

    hagateway.getColor(ld.info.ieee, ld.info.ep, 
    function(status, seq){
      //console.log("getLevel request seqNum: ", seq);
      if(status !== 0)
      {
        console.log("getLevel request confirmation failed: ", status);
      }
    });
        
    hagateway.getLevel(ld.info.ieee, ld.info.ep, 
    function(status, seq){
      //console.log("getLevel request seqNum: ", seq);
      if(status !== 0)
      {
        console.log("getLevel request confirmation failed: ", status);
      }
    });
    
    //Get state last as the state rsp triggers the write to IoT Agent
    hagateway.getState(ld.info.ieee, ld.info.ep, 
    function(status, seq){
      //console.log("getState request seqNum: ", seq);
      if(status !== 0)
      {
        console.log("getState request confirmation failed: ", status);
      }
    });

  }

  //public methods with private member access
  ld.devSet = function(data) {               
	  console.log("LightDevice ", ld.info.id, "devSet: ", data);

	  if(typeof data.reportInterval !== 'undefined')
	  {
	  	//make into whole number
	  	data.reportInterval = Math.round( data.reportInterval );
	  	
      if(ld.data.reportInterval !== data.reportInterval)
      {
      	ld.data.reportInterval = data.reportInterval;
      	clearInterval(reportTimer);
      	reportTimer = setInterval(function() { devReport(hagateway, ld); }, ld.data.reportInterval);
      }  	
	  } 
	  
	  if(typeof data.transitionTime !== 'undefined')
	  {
	    ld.data.transitionTime =  Math.round( data.transitionTime ); 	
	  }  	  
	  
	  if(typeof data.on !== 'undefined')
	  {
	  	//make into 1 or 0
	  	data.on = (data.on === 0) ? 0 : 1; 
	  	
      hagateway.setState(data.on, ld.info.ieee, ld.info.ep, 
        function(status, seq){
        //console.log("setState request seqNum: ", seq);
        if(status !== 0)
        {
          console.log("setState request confirmation failed: ", status);
        }
      });  	  	
	  }
	    	  
	  if(typeof data.level !== 'undefined')
	  {
	  	//make into whole number
	  	data.level = Math.round( data.level );
	  	if(data.level > 0xff) {
	  	  data.level = 0xff;
	  	}
	  	
      hagateway.setLevel(data.level, ld.data.transitionTime, ld.info.ieee, ld.info.ep, 
      function(status, seq){
        //console.log("setLevel request seqNum: ", seq);
        if(status !== 0)
        {
          console.log("setLevel request confirmation failed: ", status);
        }
      });   	  	
	  } 

	  if( (typeof data.hue !== 'undefined') && (typeof data.saturation !== 'undefined') )
	  {
	  	//make into whole numbers
	  	data.hue = Math.round( data.hue );
	  	data.saturation = Math.round( data.saturation );
	  	
	  	//zigbee does not allow sat of 0xff
	  	if(data.saturation > 0xfe) {
	  	  data.saturation = 0xfe;
	  	}
      hagateway.setColorWithTime(data.hue, data.saturation, ld.data.transitionTime, ld.info.ieee, ld.info.ep, 
      function(status, seq){
        console.log("setColor request seqNum: ", seq);
        if(status !== 0)
        {
          console.log("setColor request confirmation failed: ", status);
        }
      });
    }	  		         
  }
  
    //public methods with private member access
  ld.devSetInfo = function(data) {
  	ld.info.prov = data.prov;
  	console.log("devSetInfo ", ld.info.id, ": ", data.prov );

    //stored the new provisioning info
    storage.initSync();
    storage.setItem(ld.info.id, ld.info.prov);
    	
  	//replace the iotAgent with new agent that uses the new provisionin info
  	iotAgent = new IotAgent(ld.info);
  }
  
  ld.delete = function() { 
  	console.log("lightDevice: delete");
  	clearInterval(reportTimer);
  	iotAgent.delete();
  }
    
  return ld;
}

// export the class
module.exports = LightDevice;

