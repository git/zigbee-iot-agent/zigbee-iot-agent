var ByteBuffer = require("bytebuffer"); 
var Hagateway = require('../hag-client/hagateway.js');
var IotAgent = require('../iot-agent/iot-agent-qs.js');
var storage = require('node-persist');

// Constructor
function SwitchDevice(_nwkmgr, ieee, ep, type) {
  
  var sd = this;
  
  var nwkmgr = _nwkmgr;    
  

  var ieeeBb = new ByteBuffer(8, ByteBuffer.LITTLE_ENDIAN)
  .append(ieee, "hex", 0)
  .reverse();
  
  //id is first 3 byes of ieee, last 2 bytes of ieee and 
  //ep with 2 digits i.e. ep of 8 = 08
  sd.info = {
  	id: ieeeBb.toString('hex').substring(0,4) + ieeeBb.toString('hex').substring(10,16) + (("0" + ep).slice(-2)),
  	ieee: ieee,
  	ep: ep
  };
        
  //read stored provisioning info  
  storage.initSync();
  sd.info.prov = storage.getItem(sd.info.id);

  //if it is not stored point it at quickstart
  if(typeof sd.info.prov === 'undefined') {
    sd.info.prov = {
      org: "quickstart",
      type: "zb",
      auth_token: ""
    }	
  }
  
  // initialize data properties
  sd.data = { 
  	guid: ieeeBb.toString('hex') + ":" + sd.info.ep,
  	name: "ZigBee Switch", //TODO: Look at manu ID and prod ID to name it correctly 
  	type: type, 	
  	bindingTable: []
  };

  switch (type) {
    case "ColorSwitch":
      console.log("ColorSwitch not supported - creating an OnOff switch");
    case "DimmableSwitch":
      console.log("DimmableSwitch not supported - creating an OnOff switch");
    default:
      //default attributes for all light devices
      sd.data.on = 0;
      break;
  }  
    
  //console.log("New Switch: ", sd.data);  
    
  //create the connection to the HA Gateway  
  var hagateway = new Hagateway();
  hagateway
  .on('zcl-ind', function(data) {
    //console.log("SwitchDevice: got hagateway message:", data);
    
    //if its data for this device
    if( (data.ieee.toString('hex') === sd.info.ieee.toString('hex')) && (data.ep === sd.info.ep) )
    {
      //console.log("SwitchDevice: got hagateway state message");
      
      //check it is for the on/off cluster, cluster spec and not an MSP
      if( (data.clusterId === 6) &&
          (data.zclHeader.frameType === 1) &&
          (data.zclHeader.manufacturerSpecificFlag === 0) ){
      	
      	//get the command off=0, on=1, toggle=2
      	var cmd = data.zclHeader.commandId;
      	
      	//if it changed      	
      	if(sd.data.on !== cmd) {      	 
          //if switch is a toggle just toggle state
      	  if(cmd === 2) {
            if(sd.data.on === 0){
      	      sd.data.on = 1;
            }
            else{
      	      sd.data.on = 0;
      	    }
          }
          //its on or off, so just set it.
          else {
            sd.data.on = cmd; 
          } 
      	     	           	
          iotAgent.sendMessage(sd.data);         
        }
      }
      else
      {
        console.log(sd.data.guid, ": unhandled zcl indication");
      }
    }
  })
  .on('bind-ind', function(data) {
  	if(data.mode === 0) //BIND
  	{
  		sd.data.bindingTable.push({
  			ieee: data.ieee, 
  			ep: data.ep, 
  			status: data.status
  		});
  	}
  	else if(mode === 1) //UNBIND
  	{
  		for(var bindIdx = 0; bindIdx < sd.data.bindingTable.length; bindIdx++) {
  		  if( (sd.data.bindingTable[bindIdx].data.ieee === data.ieee) &&
  		      (sd.data.bindingTable[bindIdx].data.ep === data.ep) ) {
  		    sd.data.bindingTable.splice(bindIdx, 1);  		    
  		  }
  		}
  	}                    	
  });
    
  var srcIeee = sd.info.ieee;
  var srcEp = sd.info.ep;
  var dstIeee = nwkmgr.getGwInfo().ieeeAddress;
  var dstEp = nwkmgr.getGwInfo().simpleDescList[0].endpointId;
  var clusterId = 6; //OnOff cluster
   
  //create binding so GW receives event when switch state changes
  nwkmgr.setDevBinding(0, srcIeee, srcEp, dstIeee, dstEp, clusterId, 
          function(status, seq){
          //console.log("setDevBinding request seqNum: ", seq);
          
          if(status !== 0) {
            console.log("setDevBinding request confirmation failed: ", status);
          }
          else {
          	//console.log("binding cnf success");
          }
        }); 
       
  //create the connection to the IoT Agent  
  var iotAgent = new IotAgent(sd.info);
  iotAgent.on('data', function(data) {
     
  });

  //public methods with private member access
  sd.devSetInfo = function(data) {
  	sd.info.prov = data.prov;
  	console.log("devSetInfo ", sd.info.id, ": ", data.prov );

    //stored the new provisioning info
    storage.initSync();
    storage.setItem(sd.info.id, sd.info.prov);
    	
  	//replace the iotAgent with new agent that uses the new provisionin info
  	iotAgent = new IotAgent(sd.info);
  }
  
  sd.delete = function() { 
  	console.log("SwitchDevice: delete");
  	iotAgent.delete();
  }
  
}

// export the class
module.exports = SwitchDevice;

