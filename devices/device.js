var LightDevice = require('./light-device.js');
var SwitchDevice = require('./switch-device.js');
var TempSensorDevice = require('./tempsensor-device.js');
var LightSensorDevice = require('./lightsensor-device.js');

function Device(nwkmgr, ieee, simpleDesc) {
	//console.log("Device: creating new device");
  return createDevType(nwkmgr, ieee, simpleDesc);

  //private methods with private member access
  function createDevType(nwkmgr, ieee, simpleDesc)
  {
  	//console.log("Device: createDevType");
    if(//filter out HA Ep's with no clusters
      ((simpleDesc.profileId === 0x0104) &&
      ((simpleDesc.inputClusters.length > 0) ||
      (simpleDesc.outputClusters.length > 0))) ||				 				      							 
      //filter out ZLL profile with num clusters = 1 (tl ep)
      ((simpleDesc.profileId === 0xC05E) &&
      ((simpleDesc.inputClusters.length > 1) ||
      (simpleDesc.outputClusters.length > 1))))
    {	
      //HA profile 
      if(simpleDesc.profileId  === 0x0104)
      { 
        //HA Color Dimmable Light
        if(simpleDesc.deviceId === 0x0102)
        {
          return new LightDevice(nwkmgr, ieee, simpleDesc.endpointId, "ColorLight");
        }
        //HA Dimmable Light
        else if(simpleDesc.deviceId === 0x0101)
        {
          return new LightDevice(nwkmgr, ieee, simpleDesc.endpointId, "DimmableLight");
        }
        //HA OnOff Light
        else if(simpleDesc.deviceId === 0x0100)
        {
          return new LightDevice(nwkmgr, ieee, simpleDesc.endpointId, "OnOffLight");
        }
        //HA Switch
        else if(simpleDesc.deviceId === 0x0000)
        {
          return new SwitchDevice(nwkmgr, ieee, simpleDesc.endpointId, "OnOffSwitch");
        }
        //HA Color Dimmer
        else if(simpleDesc.deviceId === 0x105)
        {
          //type = "DimmerSwitch"
        }
        //HA Thermostat
        else if(simpleDesc.deviceId === 0x0301)
        {
          //type = "DimmerSwitch"
        }
        //HA Temp Sensor
        else if(simpleDesc.deviceId === 0x302)
        {
          return new TempSensorDevice(nwkmgr, ieee, simpleDesc.endpointId, "TempSensor");
        }        
        //HA Temp Sensor
        else if(simpleDesc.deviceId === 0x106)
        {
          return new LightSensorDevice(nwkmgr, ieee, simpleDesc.endpointId, "LightSensor");
        }                 
      }
      //ZLL profile
      else if(simpleDesc.profileId  === 0xC05E)
      {
        //ZLL Dimmalbe Light
        if(simpleDesc.deviceId === 0x0100)
        {
          return new LightDevice(ieee, simpleDesc.endpointId, "DimmableLight");
        }
        //ZLL Color Light
        if(simpleDesc.deviceId === 0x0200)
        {
          return new LightDevice(ieee, simpleDesc.endpointId, "ColourLight");
        }
        //ZLL Extended Color Light
        if(simpleDesc.deviceId === 0x0210)
        {
          return new LightDevice(nwkmgr, ieee, simpleDesc.endpointId, "ColorLight");
        }
        //ZLL Colour scene Remote
        if(simpleDesc.deviceId === 0x0210)
        {
          //type = ColourSceneRemote"
        }
      }
    }

    console.log("device type not recognised: ", simpleDesc.profileId, ":", simpleDesc.deviceId);
  }
}


// export the class
module.exports = Device;

