var ByteBuffer = require("bytebuffer"); 
var Hagateway = require('../hag-client/hagateway.js');
var IotAgent = require('../iot-agent/iot-agent-qs.js');
var storage = require('node-persist');

// Constructor
function LightSensorDevice(_nwkmgr, ieee, ep, type) {
  console.log("LightSensorDevice: creating new device");
  
  var ld = this;
  
  var nwkmgr = _nwkmgr;
    
  var ieeeBb = new ByteBuffer(8, ByteBuffer.LITTLE_ENDIAN)
  .append(ieee, "hex", 0)
  .reverse();
  
  //id is first 3 byes of ieee, last 2 bytes of ieee and 
  //ep with 2 digits i.e. ep of 8 = 08
  ld.info = {
  	id: ieeeBb.toString('hex').substring(0,4) + ieeeBb.toString('hex').substring(10,16) + (("0" + ep).slice(-2)),
  	ep: ep,
  	ieee: ieee
  };
        
  //read stored provisioning info  
  storage.initSync();
  ld.info.prov = storage.getItem(ld.info.id);
 
  //if it is not stored point it at quickstart
  if(typeof ld.info.prov === 'undefined') {
    ld.info.prov = {
      org: "quickstart",
      type: "zb",
      auth_token: ""
    }	
  }
    
  // initialize data properties
  ld.data = { 
  	guid: ieeeBb.toString('hex') + ":" + ld.info.ep,
  	name: "ZigBee Light Sensor", //TODO: Look at manu ID and prod ID to name it correctly 
  	type: type,
  	lumin: 0 	
  };   
  
  //create the connection to the HA Gateway  
  var hagateway = new Hagateway();
  
  //start the report
  ld.data.reportInterval = 5000; 
    
  //Afer the bind response send the configure reportInterval    
  nwkmgr.on('bind-ind', function (data) {
  	//check the bind response was for this device to the GW
  	if( (data.srcIeee.toString('hex') === ld.info.ieee.toString('hex')) && (data.srcEp === ld.info.ep) &&
  	    (data.dstIeee.toString('hex') === nwkmgr.getGwInfo().ieeeAddress.toString('hex')) && 
  	    (data.dstEp === nwkmgr.getGwInfo().simpleDescList[0].endpointId)) { 
	    //setup a report so the lightsensor sends luminance reports
		  var clusterId = 0x0400; // Luminance Measurement Cluster
		  var attrList = [];
        
		  var attrRec= {
		  	attributeId: 0, //MeasuredValue
		  	attributeType: 33, //ZCL_DATATYPE_UINT16
		  	minReportInterval: (ld.data.reportInterval / 1000),
		  	maxReportInterval: 60, //defined by HA spec
		  	reportableChange: 0 //force report to be sent on min interval
		  	//alternatively you can set reportableChange to report an temp delta 
		  	//or maxReportInterval if the delta was not reached  	
	    };
	  
	    hagateway.setAttrReport(ld.info.ieee, ld.info.ep, clusterId, [attrRec]);
    }
  });
  
  //now bind the luminance cluster of the light sensor to the GW so 
  //the reports are sent to the GW  
  var srcIeee = ld.info.ieee;
  var srcEp = ld.info.ep;
  var dstIeee = nwkmgr.getGwInfo().ieeeAddress;
  var dstEp = nwkmgr.getGwInfo().simpleDescList[0].endpointId;
  var clusterId = 0x0400; // Illuminance Measurement Cluster  
  nwkmgr.setDevBinding(0, srcIeee, srcEp, dstIeee, dstEp, clusterId, 
    function(status, seq){
    console.log("setDevBinding request seqNum: ", seq);
  
    if(status !== 0) {
      console.log("setDevBinding request confirmation failed: ", status);
    }
    else {
      //console.log("binding cnf success");
    }
  });
        
  hagateway
  .on('report', function(data) { 
  	//console.log("lightSensor report: ", data);   
    //if its data for this device and it is the luminance measurement cluster
    if( (data.ieee.toString('hex') === ld.info.ieee.toString('hex')) && 
        (data.ep === ld.info.ep) &&
        (data.clusterId === 0x400) ) {
        	
      //loop through all reports  
    	for(var reportIdx in data.attributeRecordList)
    	{     		
    	  //check it is for MeasuredValue attr ID (0) and ZCL_DATATYPE_UINT16 datatype (33)
    	  if( (data.attributeRecordList[reportIdx].attributeId === 0) &&
    	      (data.attributeRecordList[reportIdx].attributeType === 33)) {     	    
          
          var luminBb = new ByteBuffer(2, ByteBuffer.LITTLE_ENDIAN)
          .append(data.attributeRecordList[reportIdx].attributeValue, "hex", 0)
          ld.data.lumin = luminBb.readUint16();              	
          
          iotAgent.sendMessage(ld.data);
        }
      }
    }
  });

  //create the connection to the IoT Agent
  var iotAgent = new IotAgent(ld.info);
  iotAgent.on('data', function(data) {
    ld.devSet(data); 
  });
  
  //public methods with private member access
  ld.devSet = function(data) {               
	  if(typeof data.reportInterval !== 'undefined') {
	  	//make into whole number
	  	data.reportInterval = Math.round( data.reportInterval );
	  	
      if(ld.data.reportInterval !== data.reportInterval)
      {
      	ld.data.reportInterval = data.reportInterval;
      	
			  //reconfigure the report
			  var clusterId = 0x0400; // Liminance Measurement Cluster
			  var attrList = [];
			        
			  var attrRec= {
			  	attributeId: 0, //MeasuredValue
			  	attributeType: 33, //ZCL_DATATYPE_UINT16
			  	minReportInterval: (ld.data.reportInterval / 1000),
	        maxReportInterval: 60, //defined by HA spec
	        reportableChange: 0 //force report to be sent on min interval
	        //alternatively you can set reportableChange to report an lumin delta 
	        //or maxReportInterval if the delta was not reached  	
			  };
			  
			  
			  hagateway.setAttrReport(ld.info.ieee, ld.info.ep, clusterId, [attrRec]);
      }    
    }	  	
  }
  
  ld.devSetInfo = function(data) {
  	ld.info.prov = data.prov;
  	console.log("devSetInfo ", ld.info.id, ": ", data.prov );

    //stored the new provisioning info
    storage.initSync();
    storage.setItem(ld.info.id, ld.info.prov);
    	
  	//replace the iotAgent with new agent that uses the new provisionin info
  	iotAgent = new IotAgent(ld.info);
  }
  
  ld.delete = function() { 
  	console.log("LightSensorDevice: delete");
  	iotAgent.delete();
  }
}

// export the class
module.exports = LightSensorDevice;

