var Device = require('../devices/device.js');
var protobuf = require('protocol-buffers');
var fs = require("fs");
var net = require("net");
var events = require('events');

//declare out side of the scope of the function 
//so it can be used for all instances  
var nwkmgrInstance;

var PKT_HEADER_SIZE = 4;
var PKT_HEADER_LEN_FIELD = 0;
var PKT_HEADER_SUBSYS_FIELD = 2;
var PKT_HEADER_CMDID_FIELD = 3;

var NWKMGR_PORT = 2540;
  
// Constructor
function Nwkmgr() {
	
  //only create 1 nwkmgr instance
  if(typeof nwkmgrInstance !== "undefined")
  {
    return nwkmgrInstance;
  }
  nwkmgrInstance = this;
  
  events.EventEmitter.call(this);

  // initialize instance properties
  var nwkmgr_pb = protobuf(fs.readFileSync('hag-client/nwkmgr.proto'));    
  var client = net.Socket();
  var responsetHndlQ = [];
  var txQue= [];  
  var gwInfo;

  client
    .connect(NWKMGR_PORT, '127.0.0.1', function() {
      console.log('nwkmgr connected');
  });
  client
  .on('data', function(data) {

    var dataIdx = 0;
    while(dataIdx < data.length)
    {
      var rx_pkt_len = data[dataIdx + PKT_HEADER_LEN_FIELD] + (data[dataIdx + PKT_HEADER_LEN_FIELD + 1] << 8) + PKT_HEADER_SIZE;
      var ByteBuffer = require("bytebuffer");
      var rx_pkt_buf = new ByteBuffer(rx_pkt_len, ByteBuffer.LITTLE_ENDIAN);

      rx_pkt_buf.append(data.slice(dataIdx, dataIdx + rx_pkt_len), "hex", 0);
      dataIdx = dataIdx + rx_pkt_len;

      var rx_cmd_id = rx_pkt_buf.readUint8(PKT_HEADER_CMDID_FIELD);
      
      switch(rx_cmd_id) { 
        
      case nwkmgr_pb.nwkMgrCmdId_t.ZIGBEE_GENERIC_CNF:        
        var genericCnf = nwkmgr_pb.NwkZigbeeGenericCnf.decode(rx_pkt_buf.copy(PKT_HEADER_SIZE, rx_pkt_len).buffer);
        //console.log("nwkmgr genericCnf: ", genericCnf);
    
        //get the req in the queue which does not have a seqNum
        var requestHndl = getRspHndlr(-1);
        
        if(typeof requestHndl !== 'undefined')
        {
          if(genericCnf.status === nwkmgr_pb.nwkStatus_t.STATUS_SUCCESS)
          {              
            //request was confirmed, add seqNum so we can see the associated rsp.
            requestHndl.seqNum = genericCnf.sequenceNumber;
            //push back on the que
            putRspHndlr(requestHndl);
            //console.log("ZIGBEE_GENERIC_CNF", requestHndl);
            //call the callback indicating the scuccesfull confirmation of the request
            requestHndl.callback(genericCnf.status, genericCnf.sequenceNumber);            
          }
          else
          {
            //There was something wrong with the request, call the cb indicating an error	
            requestHndl.callback(genericCnf.status, -1);               	
          }
        }
        else
        {
          console.log("RspHndl for Seq ", genericCnf.sequenceNumber, "not found - status", genericCnf.status);
        }
                
        //we got a confirmation for previous message, so send the next
        txNextMessage();
                                 
        break;
    
      case nwkmgr_pb.nwkMgrCmdId_t.ZIGBEE_GENERIC_RSP_IND:
        var genericRspInd = nwkmgr_pb.NwkZigbeeGenericRspInd.decode(rx_pkt_buf.copy(PKT_HEADER_SIZE, rx_pkt_len).buffer);
        console.log("nwkmgr: ZIGBEE_GENERIC_RSP_IND");
        
        var responseHndl = getRspHndlr(genericRspInd.sequenceNumber);
        
        if(typeof responseHndl !== 'undefined')
        {
          nwkmgrInstance.emit('gen-rsp', {
            status: genericRspInd.status,
            ieee: responseHndl.reqAddr.ieeeAddr,
            ep: responseHndl.reqAddr.endpointId,        	
          });                   
        }
        else
        {
          console.log("nwkmgr RspHndl for Seq ", genericRspInd.sequenceNumber, "not found");
        }
        break;

      case nwkmgr_pb.nwkMgrCmdId_t.NWK_SET_BINDING_ENTRY_RSP_IND:
        var bindingRspInd = nwkmgr_pb.NwkSetBindingEntryRspInd.decode(rx_pkt_buf.copy(PKT_HEADER_SIZE, rx_pkt_len).buffer);
        
        var responseHndl = getRspHndlr(bindingRspInd.sequenceNumber);
        
        if(typeof responseHndl !== 'undefined')
        {
          nwkmgrInstance.emit('bind-ind', {
            status: bindingRspInd.status,
            mode: bindingRspInd.bindingMode,
            dstIeee: responseHndl.reqAddr.ieeeAddr,
            dstEp: responseHndl.reqAddr.endpointId, 
            srcIeee: bindingRspInd.srcAddr.ieeeAddr,
            srcEp: bindingRspInd.srcAddr.endpointId,                   	
          });                   
        }
        else
        {
          console.log("nwkmgr RspHndl for Seq ", bindingRspInd.sequenceNumber, "not found");
        }
        break;
                               
      case nwkmgr_pb.nwkMgrCmdId_t.NWK_GET_DEVICE_LIST_CNF:
        var devListCnfMsg = nwkmgr_pb.NwkGetDeviceListCnf.decode(rx_pkt_buf.copy(PKT_HEADER_SIZE, rx_pkt_len).buffer);
        var devIdx;

        for(devIdx in devListCnfMsg.deviceList){
          var simpleDescIdx;
          for(simpleDescIdx in devListCnfMsg.deviceList[devIdx].simpleDescList) {              
              
            var ieee = devListCnfMsg.deviceList[devIdx].ieeeAddress;//.toString("hex");
            var simpleDesc = devListCnfMsg.deviceList[devIdx].simpleDescList[simpleDescIdx];
            console.log("nwkmgr creating new device");
            var newDev = Device(nwkmgrInstance, ieee, simpleDesc);
            if(typeof newDev !== 'undefined')
            {
              nwkmgrInstance.emit('newDev', newDev);
            }
          }
        }
        break;

      case nwkmgr_pb.nwkMgrCmdId_t.NWK_ZIGBEE_DEVICE_IND:                 
        var devInd = nwkmgr_pb.NwkZigbeeDeviceInd.decode(rx_pkt_buf.copy(PKT_HEADER_SIZE, rx_pkt_len).buffer);
        //console.log("NWK_ZIGBEE_DEVICE_IND: ", devInd, " : ", nwkmgr_pb.nwkDeviceStatus_t.DEVICE_ON_LINE);
        //did it join the network
        if(devInd.deviceInfo.deviceStatus === nwkmgr_pb.nwkDeviceStatus_t.DEVICE_ON_LINE)
        {
        	//console.log("devInd:", devInd); 
          //console.log("devInd.deviceInfo.simpleDescList", devInd.deviceInfo.simpleDescList);
          var simpleDescIdx;
          for(simpleDescIdx in devInd.deviceInfo.simpleDescList) {                            
            var ieee = devInd.deviceInfo.ieeeAddress;
            var simpleDesc = devInd.deviceInfo.simpleDescList[simpleDescIdx];
            //console.log("simpleDesc:", simpleDesc); 
            var newDev = Device(nwkmgrInstance, ieee, simpleDesc);
            if(typeof newDev !== 'undefined')
            {      
            	//console.log("NWK_ZIGBEE_DEVICE_IND");    	
              nwkmgrInstance.emit('newDev', newDev);
            }
          }
        }
        else if(devInd.deviceInfo.deviceStatus === nwkmgr_pb.nwkDeviceStatus_t.DEVICE_REMOVED)
        {        	 
        	nwkmgrInstance.emit('removeDev', devInd.deviceInfo.ieeeAddress);
        }
        break;
        
      case nwkmgr_pb.nwkMgrCmdId_t.NWK_GET_LOCAL_DEVICE_INFO_CNF:        
        var gwInfoCnf = nwkmgr_pb.NwkGetLocalDeviceInfoCnf.decode(rx_pkt_buf.copy(PKT_HEADER_SIZE, rx_pkt_len).buffer);
        gwInfo = gwInfoCnf.deviceInfoList;  
        //console.log("NWK_GET_LOCAL_DEVICE_INFO_CNF: ", gwInfo);     
        break;
                                
      default:
        console.log("nwkmgr: CmdId not processed: ", rx_cmd_id);
      }
    }
  })  
  .on('error', function(err){
    console.log("Error: ", err.message);
    clientReconnect();
  });
  
  //get gw info
  getGwInfoReq();    

  // private class methods
  function clientReconnect() { 	
  	if(typeof nwkmgrInstance.clientReconnectTimer === 'undefined')
  	{  	
  		  //start a connection timer that tries to reconnect 5s,
	    nwkmgrInstance.clientReconnectTimer = setTimeout(function(){ 
		    console.log("nwkmgr clientReconnectTimer: attempting to reconnect")  		    
        client.destroy();
        client
          .connect(NWKMGR_PORT, '127.0.0.1', function() {
            console.log('nwkmgr connected');
            nwkmgrInstance.emit('connected');                       
          });
        clearTimeout(nwkmgrInstance.clientReconnectTimer);       
        delete nwkmgrInstance.clientReconnectTimer;  
		  }, 5000);
  	}  		  		  		
  }
  
  function sendMessage(msg_buf, gwAddress, responseCb) {       
    var ByteBuffer = require("bytebuffer");       
    var pkt_buf = new ByteBuffer(PKT_HEADER_SIZE + msg_buf.length, ByteBuffer.LITTLE_ENDIAN)
           .writeShort(msg_buf.length, PKT_HEADER_LEN_FIELD)
           .writeUint8(nwkmgr_pb.zStackNwkMgrSysId_t.RPC_SYS_PB_NWK_MGR, PKT_HEADER_SUBSYS_FIELD)
           .writeUint8(msg_buf[1], PKT_HEADER_CMDID_FIELD)
           .append(msg_buf, "hex", PKT_HEADER_SIZE);
    
    var responseHndl;   
    if(typeof responseCb !== 'undefined')
    {
    	console.log("sendMessage: responseHndl -1");
      responseHndl = {
      	callback: responseCb,
      	reqAddr: gwAddress,
      	seqNum: -1
      };	
    }
    else
    {
    	console.log("sendMessage: no responseHndl");
    }
    
    queTxMessage(pkt_buf, responseHndl);
  }

  function queTxMessage(pkt_buf, responseHndl) {
  	console.log("queTxMessage: queLen=", txQue.length);
    
    //if there is nothing in que then there 
  	//is no pending cnf, send now
    if(txQue.length === 0) {
    	if(typeof responseHndl !== 'undefined')
    	{
        //puh the rspHndle on the responsetHndlQ 
        putRspHndlr(responseHndl);
      }
    	console.log("queTxMessage: sending message ");
      client.write(pkt_buf.buffer); 
    }
    else {
  	  //que the message
    	var txMsg = {
    	  responseHndl: responseHndl,
    	  pkt_buf: pkt_buf
      };
    
  	  console.log("queTxMessage: queing message ");
  	  txQue.push(txMsg);
  	}
  }
    
  function txNextMessage() {  	      
    console.log("txNextMessage++")
    //get the tx message
    var txMsg = txQue.pop();
    
    //make sure there was a message in the que  
    if(typeof txMsg !== 'undefined') {
    	if(typeof responseHndl !== 'undefined')
    	{
    	  //puh the rspHndle on the responsetHndlQ 
    	  txMsg.responseHndl.id = txMsg.id;
        putRspHndlr(txMsg.responseHndl);
      }    	
    
    	console.log("txNextMessage: sending message ", txMsg.id );
      client.write(txMsg.pkt_buf.buffer); 
    } 
  }
	  	  
  function getRspHndlr(sequenceNumber)
  {
  	console.log("getRspHndlr[", responsetHndlQ.length, "]: ", sequenceNumber);
  	
    //get the queued respone handler
    if(responsetHndlQ.length > 0)    
    {
      //get response handler index for this seq num
      var rspHndlIdx;
      for(rspHndlIdx in responsetHndlQ)
      {
      	//console.log("getRspHndlr: searching", responsetHndlQ[rspHndlIdx]);
        if( responsetHndlQ[rspHndlIdx].seqNum === sequenceNumber) 
        { 	          	
          console.log("getRspHndlr found: ", sequenceNumber);
          //stop the timeout
          clearTimeout(responsetHndlQ[rspHndlIdx].timeout);
          var rsp = responsetHndlQ.splice(rspHndlIdx, 1)[0];
          console.log("getRspHndlr returning[", responsetHndlQ.length, "]: ", rsp.id, ":", rsp.seqNum);    
          return rsp;            
        }      
      }
      
      console.log("getRspHndlr: ", sequenceNumber, " not found");
    } 
  }
  
  function putRspHndlr(responseHndl)
  {
  	console.log("putRspHndlr[", responsetHndlQ.length, "]: ", responseHndl.id, ":", responseHndl.seqNum);  	
  	responseHndl.timeout = setTimeout(function(){ 
  		console.log("timeout: ", responseHndl.seqNum)
  		timeoutRspHndlr(responseHndl.seqNum);
  		}, 10000);
    responsetHndlQ.push(responseHndl);
      
  	//If response hndls are not removed something is seriously wrong
  	//The GW should always responed even if there is an error,
  	//However to protect the Q getting too long do a siple length check 
  	if(responsetHndlQ.length > 500)
  	{ 
  	  console.log("nwkmgr putRspHndlr: response Q overflow, removing oldest rspHndle");
  	  var responsetHndl = responsetHndlQ.splice(rspHndlIdx, 1)[0];  	
      responsetHndl.callback(nwkmgr_pb.nwkStatus_t.STATUS_TIMEOUT, responsetHndl);
    }
  }
  
  function timeoutRspHndlr(seqNum)
  { 
    console.log("responseHndl: ", seqNum, " timed out"); 
    responseHndl = getRspHndlr(seqNum);
    responseHndl.callback(nwkmgr_pb.nwkStatus_t.STATUS_TIMEOUT, responseHndl);
  }
       
  function getGwInfoReq() {         
    console.log("nwkmgr: getGwInfoReq");

    //create the message
    var msg_buf = nwkmgr_pb.NwkGetLocalDeviceInfoReq
      .encode({
        cmdId: nwkmgr_pb.nwkMgrCmdId_t.NWK_GET_LOCAL_DEVICE_INFO_REQ
      });
        
    sendMessage(msg_buf);      
  };
     
  // public class methods
  Nwkmgr.prototype.openNetwork = function(duraion) {        
    console.log("nwkmgr: openNetwork");

    //create the message
    var msg_buf = nwkmgr_pb.NwkSetPermitJoinReq
      .encode({
        cmdId: nwkmgr_pb.nwkMgrCmdId_t.NWK_SET_PERMIT_JOIN_REQ,
        permitJoin: nwkmgr_pb.nwkPermitJoinType_t.PERMIT_ALL,
        permitJoinTime: duraion
      });
        
    sendMessage(msg_buf);      
  };

  // public class methods
  Nwkmgr.prototype.getDevList = function() {        
    console.log("nwkmgr: getDevList");

    //create the message
    var msg_buf = nwkmgr_pb.NwkGetDeviceListReq
      .encode({
        cmdId: nwkmgr_pb.nwkMgrCmdId_t.NWK_GET_DEVICE_LIST_REQ
      });
        
    sendMessage(msg_buf);      
  };
        
  Nwkmgr.prototype.setDevBinding = function(bindingMode, srcIeee, srcEp, dstIeee, dstEp, clusterId, bindCnfCb) {            
    console.log("nwkmgr: setDevBinding");
    
    var srcBindAddress = {
      addressType: nwkmgr_pb.nwkAddressType_t.UNICAST, // Address Mode
      ieeeAddr: srcIeee,  // extended address
      endpointId: srcEp   // endpoint 
    };
  
    var dstBindAddress = {
      addressType: nwkmgr_pb.nwkAddressType_t.UNICAST, // Address Mode
      ieeeAddr: dstIeee,  // extended address
      endpointId: dstEp   // endpoint 
    };        
    
    //create the message
    var msg_buf = nwkmgr_pb.NwkSetBindingEntryReq
      .encode({
      cmdId: nwkmgr_pb.nwkMgrCmdId_t.NWK_SET_BINDING_ENTRY_REQ,
      srcAddr: srcBindAddress,
      dstAddr: dstBindAddress,
      clusterId: clusterId,
      bindingMode: bindingMode //BIND = 0, UNBIND = 1  
    });
        
    sendMessage(msg_buf, dstBindAddress, bindCnfCb);      
  };  

  Nwkmgr.prototype.removeDev = function(ieee) {            
    console.log("nwkmgr: removeDev ", ieee );
    
    var dstAddress = {
      addressType: nwkmgr_pb.nwkAddressType_t.UNICAST, // Address Mode
      ieeeAddr: ieee,  // extended address
    };
    
    //create the message
    var msg_buf = nwkmgr_pb.NwkRemoveDeviceReq
      .encode({
      cmdId: nwkmgr_pb.nwkMgrCmdId_t.NWK_REMOVE_DEVICE_REQ,
      dstAddr: dstAddress,
      leaveMode: nwkmgr_pb.nwkLeaveMode_t.LEAVE
    });
        
    sendMessage(msg_buf);      

  };
    
  Nwkmgr.prototype.getGwInfo = function() {        
    //console.log("nwkmgr: getgwInfo: ", gwInfo);
    return gwInfo;    
  };
}

Nwkmgr.prototype.__proto__ = events.EventEmitter.prototype;
 
// export the class
module.exports = Nwkmgr;


