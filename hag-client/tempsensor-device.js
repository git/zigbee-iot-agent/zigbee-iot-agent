var ByteBuffer = require("bytebuffer"); 
var Hagateway = require('../hag-client/hagateway.js');
var IotAgent = require('../iot-agent/iot-agent-qs.js');
var storage = require('node-persist');

// Constructor
function TempSensorDevice(_nwkmgr, ieee, ep, type) {
  //console.log("TempSensorDevice: creating new device");
  
  var td = this;
  
  var nwkmgr = _nwkmgr;
    
  var ieeeBb = new ByteBuffer(8, ByteBuffer.LITTLE_ENDIAN)
  .append(ieee, "hex", 0)
  .reverse();
  
  //id is first 3 byes of ieee, last 2 bytes of ieee and 
  //ep with 2 digits i.e. ep of 8 = 08
  td.info = {
  	id: ieeeBb.toString('hex').substring(0,4) + ieeeBb.toString('hex').substring(10,16) + (("0" + ep).slice(-2)),
  	ep: ep,
  	ieee: ieee
  };
        
  //read stored provisioning info  
  storage.initSync();
  td.info.prov = storage.getItem(td.info.id);
 
  //if it is not stored point it at quickstart
  if(typeof td.info.prov === 'undefined') {
    td.info.prov = {
      org: "quickstart",
      type: "zb",
      auth_token: ""
    }	
  }
    
  // initialize data properties
  td.data = { 
  	guid: ieeeBb.toString('hex') + ":" + td.info.ep,
  	name: "ZigBee Temperature Sensor", //TODO: Look at manu ID and prod ID to name it correctly 
  	type: type,
  	temp: 0 	
  };

  //start the report timer
  td.data.reportInterval = 5000;      
  
  //create the connection to the HA Gateway  
  var hagateway = new Hagateway();

  //Afer the bind response send the configure reportInterval    
  nwkmgr.on('bind-ind', function (data) {
  	
  	//check the bind response was for this device to the GW
  	if( (data.srcIeee.toString('hex') === td.info.ieee.toString('hex')) && (data.srcEp === td.info.ep) &&
  	    (data.dstIeee.toString('hex') === nwkmgr.getGwInfo().ieeeAddress.toString('hex')) && 
  	    (data.dstEp === nwkmgr.getGwInfo().simpleDescList[0].endpointId)) { 

	    //setup a report so the tempsensor sends temp reports
	    var clusterId = 0x0402; // Temperature Measurement Cluster
	    var attrList = [];
	        
	    var attrRec= {
	  	  attributeId: 0, //MeasuredValue
	  	  attributeType: 41, //ZCL_DATATYPE_INT16
	  	  minReportInterval: (td.data.reportInterval / 1000),
	  	  maxReportInterval: 60, //defined by HA spec
	  	  reportableChange: 0 //force report to be sent on min interval
	  	  //alternatively you can set reportableChange to report an temp delta 
	  	  //or maxReportInterval if the delta was not reached
	    };
	  
	    hagateway.setAttrReport(td.info.ieee, td.info.ep, clusterId, [attrRec]);
    }
  });
    
  //now send the bind request to bind the Temp cluster of the 
  //temp sensor to the GW so  
  var srcIeee = td.info.ieee;
  var srcEp = td.info.ep;
  var dstIeee = nwkmgr.getGwInfo().ieeeAddress;
  var dstEp = nwkmgr.getGwInfo().simpleDescList[0].endpointId;
  var clusterId = 0x0402; // Temperature Measurement Cluster  
  nwkmgr.setDevBinding(0, srcIeee, srcEp, dstIeee, dstEp, clusterId, 
    function(status, seq){
    //console.log("setDevBinding request seqNum: ", seq);
  
    if(status !== 0) {
      console.log("setDevBinding request confirmation failed: ", status);
    }
    else {
      //console.log("binding cnf success");
    }
  });
      
  hagateway
  .on('report', function(data) {    
    //if its data for this device and it is the temp measurement cluster
    if( (data.ieee.toString('hex') === td.info.ieee.toString('hex')) && 
        (data.ep === td.info.ep) &&
        (data.clusterId === 0x402) ) {
        	
      //loop through all reports  
    	for(var reportIdx in data.attributeRecordList)
    	{     		
    	  //check it is for MeasuredValue attr ID (0) and ZCL_DATATYPE_INT16 datatype (41)
    	  if( (data.attributeRecordList[reportIdx].attributeId === 0) &&
    	      (data.attributeRecordList[reportIdx].attributeType === 41)) {     	    
          
          var tempBb = new ByteBuffer(2, ByteBuffer.LITTLE_ENDIAN)
          .append(data.attributeRecordList[reportIdx].attributeValue, "hex", 0)
          td.data.temp = tempBb.readInt16() / 100;              	
          
          iotAgent.sendMessage(td.data);                    
        }
      }
    }
  });

  //create the connection to the IoT Agent
  var iotAgent = new IotAgent(td.info);
  iotAgent.on('data', function(data) {
    td.devSet(data)
  });
  
  //public methods with private member access
  td.devSet = function(data) {               
	  if(typeof data.reportInterval !== 'undefined')
	  {
	  	//make into whole number
	  	data.reportInterval = Math.round( data.reportInterval );
	  	
      if(td.data.reportInterval !== data.reportInterval)
      {
      	td.data.reportInterval = data.reportInterval;
      	
			  //reconfigure the report
			  var clusterId = 0x0402; // Temperature Measurement Cluster
			  var attrList = [];
			        
			  var attrRec= {
			  	attributeId: 0, //MeasuredValue
			  	attributeType: 41, //41, //ZCL_DATATYPE_INT16
			  	minReportInterval: (td.data.reportInterval / 1000),
			  	maxReportInterval: 60, //defined by HA spec
			  	reportableChange: 0 //force report to be sent on min interval
			  	//alternatively you can set reportableChange to report an temp delta 
			  	//or maxReportInterval if the delta was not reached
			  };
			  
			  
			  hagateway.setAttrReport(td.info.ieee, td.info.ep, clusterId, [attrRec]);		
      }  	
	  }
  }
  	  
  td.devSetInfo = function(data) {
  	td.info.prov = data.prov;
  	//console.log("devSetInfo ", td.info.id, ": ", data.prov );

    //stored the new provisioning info
    storage.initSync();
    storage.setItem(td.info.id, td.info.prov);
    	
  	//replace the iotAgent with new agent that uses the new provisionin info
  	iotAgent = new IotAgent(td.info);
  }
  
  td.delete = function() { 
  	console.log("TempSensorDevice: delete");
  	iotAgent.delete();
  }
}

// export the class
module.exports = TempSensorDevice;

