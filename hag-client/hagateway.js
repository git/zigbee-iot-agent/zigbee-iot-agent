var protobuf = require("protocol-buffers");
var fs = require("fs");
var net = require("net");    
  
//declare out side of the scope of the function 
//so it can be used for all instances  
var hageatwayInstance;

var PKT_HEADER_SIZE = 4;
var PKT_HEADER_LEN_FIELD = 0;
var PKT_HEADER_SUBSYS_FIELD = 2;
var PKT_HEADER_CMDID_FIELD = 3;

var HAGATEAY_PORT = 2541;

var events = require('events');
  
// Constructor
function Hagateway(_iotAgent) {
	
  //only create 1 hagateway instance
  if(typeof hageatwayInstance !== "undefined")
  {
    return hageatwayInstance;
  }
  hageatwayInstance = this;
  
  
  events.EventEmitter.call(this);
  
  // initialize instance properties
  var iotAgent = _iotAgent;
  var hagateway_pb = protobuf(fs.readFileSync('hag-client/gateway.proto'));  
  var client = net.Socket();
  var responsetHndlQ = []; 
  var txQue= [];

  console.log("hagateway: calling clientConnect"); 

  client
    .connect(HAGATEAY_PORT, '127.0.0.1', function() {
      console.log('hagateway connected');
      hageatwayInstance.emit('connected');
  });
  
  client
  .on('data', function(data) {    
    var dataIdx = 0;
    while(dataIdx < data.length)
    {
      var rx_pkt_len = data[dataIdx + PKT_HEADER_LEN_FIELD] + (data[dataIdx + PKT_HEADER_LEN_FIELD + 1] << 8) + PKT_HEADER_SIZE; 
      var ByteBuffer = require("bytebuffer");
      var rx_pkt_buf = new ByteBuffer(rx_pkt_len, ByteBuffer.LITTLE_ENDIAN);
      
      rx_pkt_buf.append(data.slice(dataIdx, dataIdx + rx_pkt_len), "hex", 0);
      dataIdx = dataIdx + rx_pkt_len;

      var rx_cmd_id = rx_pkt_buf.readUint8(PKT_HEADER_CMDID_FIELD);
            
      switch(rx_cmd_id) { 
      	
      case hagateway_pb.gwCmdId_t.ZIGBEE_GENERIC_CNF:
        var genericCnf = hagateway_pb.GwZigbeeGenericCnf.decode(rx_pkt_buf.copy(PKT_HEADER_SIZE, rx_pkt_len).buffer);
        //console.log("genericCnf: ", genericCnf);
        
        //get the first req in the queue which does not have a seqNum
        var requestHndl = getRspHndlr(-1);
        
        if(typeof requestHndl !== 'undefined')
        {
          if(genericCnf.status === hagateway_pb.gwStatus_t.STATUS_SUCCESS)
          {              
            //request was confirmed, add seqNum so we can see the associated rsp.
            requestHndl.seqNum = genericCnf.sequenceNumber;
            //push back on the que
            putRspHndlr(requestHndl);
            //console.log("ZIGBEE_GENERIC_CNF", requestHndl);
            //call the callback indicating the scuccesfull confirmation of the request
            requestHndl.callback(genericCnf.status, genericCnf.sequenceNumber);            
          }
          else
          {
            //There was something wrong with the request, call the cb indicating an error	
            requestHndl.callback(genericCnf.status, -1);               	
          }
        }
        else
        {
          console.log("RspHndl for Seq ", genericCnf.sequenceNumber, "not found - status", genericCnf.status);
        }  
        
        //we got a confirmation for previous message, so send the next
        txNextMessage();
                
        break;
  
      case hagateway_pb.gwCmdId_t.ZIGBEE_GENERIC_RSP_IND:
        var gwZigbeeGenericRspInd = hagateway_pb.GwZigbeeGenericRspInd.decode(rx_pkt_buf.copy(PKT_HEADER_SIZE, rx_pkt_len).buffer);
        //console.log("hagateway: ZIGBEE_GENERIC_RSP_IND");
      
        var responseHndl = getRspHndlr(gwZigbeeGenericRspInd.sequenceNumber);
      
        if(typeof responseHndl !== 'undefined')
        {
          hageatwayInstance.emit('gen-rsp', {
            status: gwZigbeeGenericRspInd.status,
            ieee: responseHndl.reqAddr.ieeeAddr,
            ep: responseHndl.reqAddr.endpointId,        	
          });                   
        }
        else
        {
          console.log("RspHndl for Seq ", gwZigbeeGenericRspInd.sequenceNumber, "not found");
        }
        break;
                    
      case hagateway_pb.gwCmdId_t.GW_ZCL_FRAME_RECEIVE_IND:
        var gwZclFrameReceiveInd = hagateway_pb.GwZclFrameReceiveInd.decode(rx_pkt_buf.copy(PKT_HEADER_SIZE, rx_pkt_len).buffer);
        //console.log("hagateway: GW_ZCL_FRAME_RECEIVE_IND");
          
        var zclHeader = {
          frameType: gwZclFrameReceiveInd.frameType,
          manufacturerSpecificFlag: gwZclFrameReceiveInd.manufacturerSpecificFlag,
          manufacturerCode: gwZclFrameReceiveInd.manufacturerCode,
          clientServerDirection: gwZclFrameReceiveInd.clientServerDirection,
          disableDefaultRsp: gwZclFrameReceiveInd.disableDefaultRsp,
          commandId: gwZclFrameReceiveInd.commandId   	
        };
        
        //send the zclFrame to the devices
        hageatwayInstance.emit('zcl-ind', {
            ieee: gwZclFrameReceiveInd.srcAddress.ieeeAddr,
            ep: gwZclFrameReceiveInd.srcAddress.endpointId,
            clusterId: gwZclFrameReceiveInd.clusterId, 
            zclHeader: zclHeader,         
            payload: gwZclFrameReceiveInd.payload            
          });                             
        break;

      case hagateway_pb.gwCmdId_t.GW_SET_ATTRIBUTE_REPORTING_RSP_IND:
        var getAttrReportRsp = hagateway_pb.GwSetAttributeReportingRspInd.decode(rx_pkt_buf.copy(PKT_HEADER_SIZE, rx_pkt_len).buffer);
        //console.log("hagateway: GW_SET_ATTRIBUTE_REPORTING_RSP_IND");
      
        var responseHndl = getRspHndlr(getAttrReportRsp.sequenceNumber);
      
        if(typeof responseHndl !== 'undefined')
        {          	
          hageatwayInstance.emit('attrreport-ind', {
            status: getAttrReportRsp.status,
            seqNum: getAttrReportRsp.sequenceNumber,
          });
        }
        else
        {
          console.log("RspHndl for Seq ", getAttrReportRsp.sequenceNumber, "not found");
        }
        break; 
                      
      case hagateway_pb.gwCmdId_t.DEV_GET_ONOFF_STATE_RSP_IND:
        var getDevStateRsp = hagateway_pb.DevGetOnOffStateRspInd.decode(rx_pkt_buf.copy(PKT_HEADER_SIZE, rx_pkt_len).buffer);
        //console.log("hagateway: DEV_GET_ONOFF_STATE_RSP_IND");
      
        var responseHndl = getRspHndlr(getDevStateRsp.sequenceNumber);
      
        if(typeof responseHndl !== 'undefined')
        {          	
          if(getDevStateRsp.status === hagateway_pb.gwStatus_t.STATUS_SUCCESS) 
          {
            var on = getDevStateRsp.stateValue;
          }
          else
          {
            var on = 0;
          } 

          hageatwayInstance.emit('state', {
            status: getDevStateRsp.status,
            seqNum: getDevStateRsp.sequenceNumber,
            ieee: responseHndl.reqAddr.ieeeAddr,
            ep: responseHndl.reqAddr.endpointId,
            on: on
          });
        }
        else
        {
          console.log("RspHndl for Seq ", getDevStateRsp.sequenceNumber, "not found");
        }
        break;                           

      case hagateway_pb.gwCmdId_t.DEV_GET_LEVEL_RSP_IND:
        var getDevLevelRsp = hagateway_pb.DevGetLevelRspInd.decode(rx_pkt_buf.copy(PKT_HEADER_SIZE, rx_pkt_len).buffer);
        //console.log("hagateway: DEV_GET_LEVEL_RSP_IND");
      
        var responseHndl = getRspHndlr(getDevLevelRsp.sequenceNumber);
      
        if(typeof responseHndl !== 'undefined')
        {
          if(getDevLevelRsp.status === hagateway_pb.gwStatus_t.STATUS_SUCCESS) 
          {
            var level = getDevLevelRsp.levelValue;
          }
          else
          {
           var level = 0;
          } 
          hageatwayInstance.emit('level', {
            status: getDevLevelRsp.status,
            seqNum: getDevLevelRsp.sequenceNumber,
            ieee: responseHndl.reqAddr.ieeeAddr,
            ep: responseHndl.reqAddr.endpointId,
            level: level
          });
        }
        else
        {
          console.log("RspHndl for Seq ", getDevLevelRsp.sequenceNumber, "not found");
        }
        break;                           

      case hagateway_pb.gwCmdId_t.DEV_GET_COLOR_RSP_IND:
        var getDevColorRsp = hagateway_pb.DevGetColorRspInd.decode(rx_pkt_buf.copy(PKT_HEADER_SIZE, rx_pkt_len).buffer);
        //console.log("hagateway: DEV_GET_COLOR_RSP_IND");
      
        var responseHndl = getRspHndlr(getDevColorRsp.sequenceNumber);
      
        if(typeof responseHndl !== 'undefined')
        {
          if(getDevColorRsp.status === hagateway_pb.gwStatus_t.STATUS_SUCCESS) 
          {
            var hue = getDevColorRsp.hueValue;
            var saturation = getDevColorRsp.satValue;
          }
          else
          {
            var hue = 0;
            var saturation = 0;
          } 
          
          hageatwayInstance.emit('color', {
            status: getDevColorRsp.status,
            seqNum: getDevColorRsp.sequenceNumber,
            ieee: responseHndl.reqAddr.ieeeAddr,
            ep: responseHndl.reqAddr.endpointId,
            hue: hue,
            saturation: saturation
          });
        }
        else
        {
          console.log("RspHndl for Seq ", getDevColorRsp.sequenceNumber, "not found");
        }
        break; 

      case hagateway_pb.gwCmdId_t.DEV_GET_TEMP_RSP_IND:
        var devGetTempRspInd = hagateway_pb.DevGetTempRspInd.decode(rx_pkt_buf.copy(PKT_HEADER_SIZE, rx_pkt_len).buffer);
        //console.log("hagateway: DEV_GET_TEMP_RSP_IND");
      
        var responseHndl = getRspHndlr(devGetTempRspInd.sequenceNumber);
      
        if(typeof responseHndl !== 'undefined')
        {          	
          if(devGetTempRspInd.status === hagateway_pb.gwStatus_t.STATUS_SUCCESS) 
          {
            var on = devGetTempRspInd.stateValue;
          }
          else
          {
            var on = 0;
          } 

          hageatwayInstance.emit('temp', {
            status: devGetTempRspInd.status,
            seqNum: devGetTempRspInd.sequenceNumber,
            ieee: responseHndl.reqAddr.ieeeAddr,
            ep: responseHndl.reqAddr.endpointId,
            temp: (devGetTempRspInd.temperatureValue / 100)
          });
        }
        else
        {
          console.log("RspHndl for Seq ", devGetTempRspInd.sequenceNumber, "not found");
        }
        break;    
            
      case hagateway_pb.gwCmdId_t.GW_ATTRIBUTE_REPORTING_IND:
        //console.log("GW_ATTRIBUTE_REPORTING_IND");
        var attributeReportingInd = hagateway_pb.GwAttributeReportingInd.decode(rx_pkt_buf.copy(PKT_HEADER_SIZE, rx_pkt_len).buffer);
        //console.log("attributeReportingIndBuffer decoded: ", attributeReportingInd);
        
        hageatwayInstance.emit('report', {
            ieee: attributeReportingInd.srcAddress.ieeeAddr,
            ep: attributeReportingInd.srcAddress.endpointId,
            clusterId: attributeReportingInd.clusterId,
            attributeRecordList: attributeReportingInd.attributeRecordList
        });
        
        break;    
   
      default:
        console.log("hagateway: CmdId not processed: ", rx_cmd_id);
      }
    }
  })
  .on('error', function(err){
    console.log("Error: ", err.message);
    clientReconnect();
  });    

  // private class methods
  function clientReconnect() { 	
  	if(typeof hageatwayInstance.clientReconnectTimer === 'undefined')
  	{  	
  		  //start a connection timer that tries to reconnect 5s,
	    hageatwayInstance.clientReconnectTimer = setTimeout(function(){ 
		    console.log("hagateway clientReconnectTimer: attempting to reconnect")  		    
        client
          .connect(HAGATEAY_PORT, '127.0.0.1', function() {
            console.log('hagateway connected');
            hageatwayInstance.emit('connected');                       
          });
        clearTimeout(hageatwayInstance.clientReconnectTimer);       
        delete hageatwayInstance.clientReconnectTimer;  
		  }, 5000);
  	}  		  		  		
  }
  	
  // private class methods
  function sendMessage(msg_buf, gwAddress, responseCb) {       
    var ByteBuffer = require("bytebuffer");       
    var pkt_buf = new ByteBuffer(PKT_HEADER_SIZE + msg_buf.length, ByteBuffer.LITTLE_ENDIAN)
           .writeShort(msg_buf.length, PKT_HEADER_LEN_FIELD)
           .writeUint8(hagateway_pb.zStackGwSysId_t.RPC_SYS_PB_GW, PKT_HEADER_SUBSYS_FIELD)
           .writeUint8(msg_buf[1], PKT_HEADER_CMDID_FIELD)
           .append(msg_buf, "hex", PKT_HEADER_SIZE);
    
    var responseHndl;   
    if(typeof responseCb !== 'undefined')
    {
    	//console.log("sendMessage: responseHndl -1");
      responseHndl = {
      	callback: responseCb,
      	reqAddr: gwAddress,
      	seqNum: -1
      };	
    }
    else
    {
    	console.log("sendMessage: no responseHndl");
    }
    
    queTxMessage(pkt_buf, responseHndl);
  }

  function queTxMessage(pkt_buf, responseHndl) {
  	//console.log("queTxMessage: queLen=", txQue.length);
    
    //if there is nothing in que then there 
  	//is no pending cnf, send now
    if(txQue.length === 0) {
    	if(typeof responseHndl !== 'undefined')
    	{
        //puh the rspHndle on the responsetHndlQ 
        putRspHndlr(responseHndl);
      }
    	//console.log("queTxMessage: sending message ");
      client.write(pkt_buf.buffer); 
    }
    else {
  	  //que the message
    	var txMsg = {
    	  responseHndl: responseHndl,
    	  pkt_buf: pkt_buf
      };
    
  	  //console.log("queTxMessage: queing message ");
  	  txQue.push(txMsg);
  	}
  }
    
  function txNextMessage() {  	      
    //console.log("txNextMessage++")
    //get the tx message
    var txMsg = txQue.pop();
    
    //make sure there was a message in the que  
    if(typeof txMsg !== 'undefined') {
    	if(typeof responseHndl !== 'undefined')
    	{
    	  //puh the rspHndle on the responsetHndlQ 
    	  txMsg.responseHndl.id = txMsg.id;
        putRspHndlr(txMsg.responseHndl);
      }    	
    
    	//console.log("txNextMessage: sending message ", txMsg.id );
      client.write(txMsg.pkt_buf.buffer); 
    } 
  }

	  	  
  function getRspHndlr(sequenceNumber)
  {
  	//console.log("getRspHndlr[", responsetHndlQ.length, "]: ", sequenceNumber);
  	
    //get the queued respone handler
    if(responsetHndlQ.length > 0)    
    {
      //get response handler index for this seq num
      var rspHndlIdx;
      for(rspHndlIdx in responsetHndlQ)
      {
      	//console.log("getRspHndlr: searching", responsetHndlQ[rspHndlIdx]);
        if( responsetHndlQ[rspHndlIdx].seqNum === sequenceNumber) 
        { 	          	
          //console.log("getRspHndlr found: ", sequenceNumber);
          //stop the timeout
          clearTimeout(responsetHndlQ[rspHndlIdx].timeout);
          var rsp = responsetHndlQ.splice(rspHndlIdx, 1)[0];
          //console.log("getRspHndlr returning[", responsetHndlQ.length, "]: ", rsp.id, ":", rsp.seqNum);    
          return rsp;            
        }      
      }
      
      //console.log("getRspHndlr: ", sequenceNumber, " not found");
    } 
  }
  
  function putRspHndlr(responseHndl)
  {
  	//console.log("putRspHndlr[", responsetHndlQ.length, "]: ", responseHndl.id, ":", responseHndl.seqNum);  	
  	responseHndl.timeout = setTimeout(function(){ 
  		console.log("timeout: ", responseHndl.seqNum)
  		timeoutRspHndlr(responseHndl.seqNum);
  		}, 10000);
    responsetHndlQ.push(responseHndl);
      
  	//If response hndls are not removed something is seriously wrong
  	//The GW should always responed even if there is an error,
  	//However to protect the Q getting too long do a siple length check 
  	if(responsetHndlQ.length > 500)
  	{ 
  	  //console.log("hagateway putRspHndlr: response Q overflow, removing oldest rspHndle");
  	  var responsetHndl = responsetHndlQ.splice(rspHndlIdx, 1)[0];  	
      responsetHndl.callback(hagateway_pb.gwStatus_t.STATUS_TIMEOUT, responsetHndl);
    }
  }
  
  function timeoutRspHndlr(seqNum)
  { 
    console.log("responseHndl: ", seqNum, " timed out"); 
    responseHndl = getRspHndlr(seqNum);
    responseHndl.callback(hagateway_pb.gwStatus_t.STATUS_TIMEOUT, responseHndl);
  }
   
  // public class methods
  Hagateway.prototype.setState = function(state, ieee, ep, requestCb) {        
    //console.log("hagateway setState: ", ieee.toString('hex'), ":", ep, " - ", state);

    //create the message
    var gwAddress = {
      addressType: hagateway_pb.gwAddressType_t.UNICAST, // Address Mode
      ieeeAddr: ieee,  // extended address
      endpointId: ep   // endpoint 
    };

    var devSetOnOffStateReq = hagateway_pb.DevSetOnOffStateReq
      .encode({
        cmdId: hagateway_pb.gwCmdId_t.DEV_SET_ONOFF_STATE_REQ,
        state: state,
        dstAddress: gwAddress
      });
        
    sendMessage(devSetOnOffStateReq, gwAddress, requestCb);      

  };
  
  Hagateway.prototype.getState = function(ieee, ep, requestCb) {        
    //console.log("hagateway getState: ", ieee.toString('hex'), ":", ep);

    //create the message
    var gwAddress = {
      addressType: hagateway_pb.gwAddressType_t.UNICAST, // Address Mode
      ieeeAddr: ieee,  // extended address
      endpointId: ep   // endpoint 
    };

    var gevGetOnOffStateReq = hagateway_pb.DevGetOnOffStateReq
      .encode({
        cmdId: hagateway_pb.gwCmdId_t.DEV_GET_ONOFF_STATE_REQ,
        dstAddress: gwAddress
      });
        
    sendMessage(gevGetOnOffStateReq, gwAddress, requestCb);      

  };

  Hagateway.prototype.setLevel = function(level, transitionTime, ieee, ep, requestCb) {        
    console.log("hagateway setLevel: ", ieee.toString('hex'), ":", ep, " - ", level);

    //create the message
    var gwAddress = {
      addressType: hagateway_pb.gwAddressType_t.UNICAST, // Address Mode
      ieeeAddr: ieee,  // extended address
      endpointId: ep   // endpoint 
    };

    var gevSetLevelReq = hagateway_pb.DevSetLevelReq
      .encode({
        cmdId: hagateway_pb.gwCmdId_t.DEV_SET_LEVEL_REQ,
        levelValue: level,
        //Tsnistion time in ZigBee is measure in 100ms, change from 1ms
        transitionTime: transitionTime/100,
        dstAddress: gwAddress
      });
        
    sendMessage(gevSetLevelReq, gwAddress, requestCb);      

  };
    
  Hagateway.prototype.getLevel = function(ieee, ep, requestCb) {        
    //console.log("hagateway getLevel: ", ieee.toString('hex'), ":", ep);

    //create the message
    var gwAddress = {
      addressType: hagateway_pb.gwAddressType_t.UNICAST, // Address Mode
      ieeeAddr: ieee,  // extended address
      endpointId: ep   // endpoint 
    };

    var gevGetLevelReq = hagateway_pb.DevGetLevelReq
      .encode({
        cmdId: hagateway_pb.gwCmdId_t.DEV_GET_LEVEL_REQ,
        dstAddress: gwAddress
      });
        
    sendMessage(gevGetLevelReq, gwAddress, requestCb);      

  };
  
  Hagateway.prototype.setColor = function(hue, saturation, ieee, ep, requestCb) {        
    //console.log("hagateway setColor: ", ieee.toString('hex'), ":", ep, " - ", hue, ":", saturation);

    //create the message
    var gwAddress = {
      addressType: hagateway_pb.gwAddressType_t.UNICAST, // Address Mode
      ieeeAddr: ieee,  // extended address
      endpointId: ep   // endpoint 
    };

    var gevSetColorReq = hagateway_pb.DevSetColorReq
      .encode({
        cmdId: hagateway_pb.gwCmdId_t.DEV_SET_COLOR_REQ,
        hueValue: hue,
        saturationValue: saturation,
        dstAddress: gwAddress
      });
        
    sendMessage(gevSetColorReq, gwAddress, requestCb);      

  };
    
  Hagateway.prototype.getColor = function(ieee, ep, requestCb) {        
    //console.log("hagateway getColor: ", ieee.toString('hex'), ":", ep);

    //create the message
    var gwAddress = {
      addressType: hagateway_pb.gwAddressType_t.UNICAST, // Address Mode
      ieeeAddr: ieee,  // extended address
      endpointId: ep   // endpoint 
    };

    var gevGetColorReq = hagateway_pb.DevGetColorReq
      .encode({
        cmdId: hagateway_pb.gwCmdId_t.DEV_GET_COLOR_REQ,
        dstAddress: gwAddress
      });
        
    sendMessage(gevGetColorReq, gwAddress, requestCb);      

  }; 
  
  Hagateway.prototype.sendRawZcl = function(dstAddress, 
              profileId, clusterId, zclHeader, zclPayload, requestCb) { 
              	           
    var gwSendZclFrameReq = hagateway_pb.GwSendZclFrameReq
      .encode({
        cmdId: hagateway_pb.gwCmdId_t.GW_SEND_ZCL_FRAME_REQ,
        dstAddress: dstAddress,
        endpointIdSource: 2,
        profileId: profileId,
        qualityOfService: hagateway_pb.gwQualityOfService_t.APS_NOT_ACK,
        securityOptions: hagateway_pb.gwSecurityOptions_t.APS_SECURITY_DISABLED,
        clusterId: clusterId,
        frameType: zclHeader.frameType,
        manufacturerSpecificFlag: zclHeader.manufacturerSpecificFlag,
        //manufacturerCode: zclHeader.manufacturerCode,	
        clientServerDirection: zclHeader.clientServerDirection,
        disableDefaultRsp: zclHeader.disableDefaultRsp,
        commandId: zclHeader.commandId,
        payload: zclPayload.buffer         
      });
        
    sendMessage(gwSendZclFrameReq, dstAddress, requestCb);      

  };   
  
  Hagateway.prototype.setColorWithTime = function(hue, saturation, tranisTime, ieee, ep, requestCb) {        
    //console.log("hagateway setColorWithTime: ", ieee.toString('hex'), ":", ep, " - ", hue, ":", saturation);

    //create the message
    var dstAddress = {
      addressType: hagateway_pb.gwAddressType_t.UNICAST, // Address Mode
      ieeeAddr: ieee,  // extended address
      endpointId: ep   // endpoint 
    };

    var zclHeader = {
        frameType: hagateway_pb.gwFrameType_t.FRAME_CLUSTER_SPECIFIC,
        manufacturerSpecificFlag: hagateway_pb.gwMfrSpecificFlag_t.NON_MFR_SPECIFIC,
        clientServerDirection: hagateway_pb.gwClientServerDir_t.CLIENT_TO_SERVER,
        disableDefaultRsp: hagateway_pb.gwDisableDefaultRsp_t.DEFAULT_RSP_ENABLED,
        commandId: 0x06 //MOVE_TO_HUE_AND_SATURATION    	
    };
    
    var ByteBuffer = require("bytebuffer"); 
    var zclPayload = new ByteBuffer(4, ByteBuffer.LITTLE_ENDIAN)           
           .writeUint8(hue, 0)
           .writeUint8(saturation, 1)
           .writeShort(tranisTime, 2);
        
    hageatwayInstance.sendRawZcl(dstAddress, 0x0104, 0x0300, zclHeader, zclPayload, requestCb);  

  };
 
  Hagateway.prototype.getTemp = function(ieee, ep, requestCb) {        
    //console.log("hagateway getTemp: ", ieee.toString('hex'), ":", ep);

    //create the message
    var dstAddress = {
      addressType: hagateway_pb.gwAddressType_t.UNICAST, // Address Mode
      ieeeAddr: ieee,  // extended address
      endpointId: ep   // endpoint 
    };

    var devGetTempReq = hagateway_pb.DevGetTempReq
      .encode({
        cmdId: hagateway_pb.gwCmdId_t.DEV_GET_TEMP_REQ,
        dstAddress: dstAddress
      });
        
    sendMessage(devGetTempReq, dstAddress, requestCb);      

  };   
   
  Hagateway.prototype.setAttrReport = function(ieee, ep, clusterId, attrList, requestCb) {        

    //create the message
    var dstAddress = {
      addressType: hagateway_pb.gwAddressType_t.UNICAST, // Address Mode
      ieeeAddr: ieee,  // extended address
      endpointId: ep   // endpoint 
    };
    
    //bounds checking - minReportInterval can not be les than 1
    var attrIdx;
    for(attrIdx in attrList) {
    	if(attrList[attrIdx].minReportInterval < 1) attrList[attrIdx].minReportInterval = 1;
    }
    
    console.log("hagateway setAttrReport: ", {
        cmdId: hagateway_pb.gwCmdId_t.GW_SET_ATTRIBUTE_REPORTING_REQ,
        dstAddress: dstAddress,
        clusterId: clusterId,
        attributeReportList: attrList         
    });
    
        
    var setAttributeReportingReq = hagateway_pb.GwSetAttributeReportingReq
      .encode({
        cmdId: hagateway_pb.gwCmdId_t.GW_SET_ATTRIBUTE_REPORTING_REQ,
        dstAddress: dstAddress,
        clusterId: clusterId,
        attributeReportList: attrList         
      });
     
    sendMessage(setAttributeReportingReq, dstAddress, requestCb);      

  }; 
}

Hagateway.prototype.__proto__ = events.EventEmitter.prototype;
 
// export the class
module.exports = Hagateway;


