#ZigBee IoT Kit

The SW contained on this git is intended to be used in conjunction with the ZigBee IoT Kit. This SW is a client to the ZigBee Home Automation Gateway described [here](http://www.ti.com/tool/CC2531EM-IOT-HOME-GATEWAY-RD) and available to download [here](http://www.ti.com/tool/z-stack) (Z-STACK-LINUX-GATEWAY download).


**TBD: WILL HW BE BROUGHT SEPARATELY OR AS A KIT? Links here!!!**

In this kit you will receive a USB key that will install all the required SW on a BeagleBone Black, clone this git and resolve the NPM dependencies.
 
#Setting up the BeagleBone Black#

To set the beaglebone Black as a Linux ZigBee Gateway:

1. Connect the USB memory key provided to the Beaglebone Black
2. Connect the Ethernet port of the BeagleBone Black to an Access Point that has a direct connection to the internet.
2. Connect the Beaglebone Black USB to your PC and wait for it to bootup.
3. Open [Tera-Term](http://en.sourceforge.jp/projects/ttssh2/releases/) or similar serail terminal. 
4. If the Beaglebone has booted and the [Gadget Enet Drivers for BeagleBone](http://beagleboard.org/static/beaglebone/latest/README.htm#step2) have been installed correctly you will see "COMxxx: Gadget Serial (COMxx)" as an available serial port. 
5. Open the "COMxxx: Gadget Serial (COMxx)" Serial port.
5. Select "Setup -> Serial port..." from the menu bar and set it to 115200 8n1 with no flow control. 
4. Select "Setup -> Terminal..." from the menu bar and ensure that "New-Line" - "Receive:" and "Transmit:" are set to "CR" and that "Local Echo" is off.
6. Press enter to get a prompt and enter "root" as the username and no password.
7. Run the setup script from the USB Memory Stick:    
    
    	/media/setup/setup.sh    
9. Once the script has run halt the BeagleBone Black by typing `halt` in the command line.
 
#Running The IoT Agent And Web Server#

In the following sections you will be walking through the ZigBee IoT kit out of box experience. 

The IoT Agent and web servers are implemented using Node.js. The web server allow you to interact with the ZigBee devices locally with no internet connect. The IoT agent connects to the IBM IoT service.

1. When the Beaglebone Black has halt remove the USB memory key and connect the CC2531 USB Dongle programmed with the ZNP FW and then power cycle the BeagleBone.

The gadget driver can be temperamental if it does not reappear press reset on BeagleBone Black, remove the USB close Teraterm and reconnect the USB and wait for it to reboot.

2. When the BeagleBone Black reboots it should be running the Zstack Linux Gateway as a service. To check this is running correct:
   
    	systemctl status zbgateway.service
      

	if everything is setup correctly it will repoort:

    	May 15 02:26:02 beaglebone zigbeegw[1436]: starting tracker with our pid /ro...2
		May 15 02:26:02 beaglebone zigbeegw[1436]: =================================...=
    	May 15 02:26:02 beaglebone zigbeegw[1436]: tracking 4 pids, 1504 1510 1523 1532
    	May 15 02:26:02 beaglebone zigbeegw[1436]: when we see something missing we ...6
    	May 15 02:45:01 beaglebone-zigbeegw[1815]: starting tracker with our p... 

2. The IoT agent should also be running as a separate service. To check this is running correct:
   
    	systemctl status zbiotagent.service
      
	if everything is setup correctly it will repoort:

    	May 15 02:26:02 beaglebone zigbeegw[1436]: starting tracker with our pid /ro...2
		May 15 02:26:02 beaglebone zigbeegw[1436]: =================================...=
    	May 15 02:26:02 beaglebone zigbeegw[1436]: tracking 4 pids, 1504 1510 1523 1532
    	May 15 02:26:02 beaglebone zigbeegw[1436]: when we see something missing we ...6
    	May 15 02:45:01 beaglebone-zigbeegw[1815]: starting tracker with our p... 
 
3. get the IP address of the BeagleBone Black (in this case it is 192.168.100.104):

   
    	# ifconfig
    	eth0  Link encap:Ethernet  HWaddr c8:a0:30:ac:4d:bf
      	inet addr:192.168.100.104  Bcast:192.168.100.255  Mask:255.255.255.0
      	inet6 addr: fe80::caa0:30ff:feac:4dbf/64 Scope:Link
      	UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
      	RX packets:2326 errors:0 dropped:0 overruns:0 frame:0
      	TX packets:130 errors:0 dropped:0 overruns:0 carrier:0
      	collisions:0 txqueuelen:1000
      	RX bytes:746806 (729.3 KiB)  TX bytes:16904 (16.5 KiB)
      	Interrupt:40
    
    	loLink encap:Local Loopback
      	inet addr:127.0.0.1  Mask:255.0.0.0
      	inet6 addr: ::1/128 Scope:Host
      	UP LOOPBACK RUNNING  MTU:65536  Metric:1
      	RX packets:2252 errors:0 dropped:0 overruns:0 frame:0
      	TX packets:2252 errors:0 dropped:0 overruns:0 carrier:0
      	collisions:0 txqueuelen:0
      	RX bytes:171340 (167.3 KiB)  TX bytes:171340 (167.3 KiB)
    
    	usb0  Link encap:Ethernet  HWaddr ea:ac:35:eb:82:52
      	inet addr:192.168.7.2  Bcast:192.168.7.3  Mask:255.255.255.252
      	inet6 addr: fe80::e8ac:35ff:feeb:8252/64 Scope:Link
      	UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
      	RX packets:0 errors:0 dropped:0 overruns:0 frame:0
      	TX packets:0 errors:0 dropped:0 overruns:0 carrier:0
      	collisions:0 txqueuelen:1000
      	RX bytes:0 (0.0 B)  TX bytes:0 (0.0 B)
    
5.  Check that you are on the same LAN as the BeagleBone Black, then browse to http://<*ip_address*>:5000 and view the local web interface. You should see:

![](http://processors.wiki.ti.com/images/6/6d/Devlist-1.JPG)

#Connecting The Sensor Tag#

1. In the web interface press the "Add Device" button to open the network.
2. Insert the battery into the Sensor Tag.
3. Observe the Sensor Tag joining the network and appearing in the device list of the web interface. When the device has joined click "Done".
4. Click on the temperature sensor in the device list. You should see a web interface which reflects the temperature measured.
5. Click on the menu icon in the top right and then "Back" to get back to the device list.

The ZigBee Sensor Tag has 3 end points:

- Light Sensor
- Temperature Sensor
- Switch 

Each EndPoint is represented as a device in the web interface. Spend a few minutes exploring the web interface for each device.

![](http://processors.wiki.ti.com/images/f/f9/Devview-1.JPG)

#Viewing The Devices Data In IBM QuickStart#

Each device can be visualized in the IBM QuickStart service, the devices start publishing their data to the IBM QuickStart Service when they join the network. To view the Quickstart page for each device simply click on the device, click on the menu icon of the devices web page and click "QuickStart Link".

![](http://processors.wiki.ti.com/images/4/44/Qstemp-1.JPG) 



